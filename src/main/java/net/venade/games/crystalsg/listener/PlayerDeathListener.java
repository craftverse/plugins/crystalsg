package net.venade.games.crystalsg.listener;

import lombok.RequiredArgsConstructor;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.manager.npc.NpcEntitySpawner;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.game.GamePrefix;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author JakeMT04
 * @since 18/08/2021
 */
@RequiredArgsConstructor
public class PlayerDeathListener implements Listener {
  private final GameManager manager;

  @EventHandler
  public void onRespawn(PlayerRespawnEvent event) {
    CrystalSGPlayer player = this.manager.getPlayers().get(event.getPlayer().getUniqueId());
    if (player == null) {
      return;
    }
    event.setRespawnLocation(player.getDeathLocation().clone().add(0, 2, 0));
    this.manager.applySpectatorSettings(player, "crystal.spec.reason.died");
  }

  @EventHandler
  public void onDeath(PlayerDeathEvent event) {
    // npc deaths are handled by the NpcListener
    if (NpcEntitySpawner.isNpc(event.getEntity())) {
      return;
    }
    CrystalSGPlayer player = this.manager.getPlayers().get(event.getEntity().getUniqueId());
    if (player == null) {
      return;
    }
    player.saveInventory();
    player.getBase().getLocation().clone();
    player.createHead(player.getBase().getLocation().clone());
    player.getBase().setBedSpawnLocation(null);
    player.setState(CrystalSGPlayer.PlayerState.DEAD);
    player.getBase().getWorld().strikeLightningEffect(player.getBase().getLocation());
    event.setDeathMessage(null);
    String message =
        GamePrefix.DEATH_PREFIX + MiscUtils.getOtherDeathMessage(player.getBase()) + "§f.";
    if (event.getEntity().getKiller() != null) {
      CrystalSGPlayer killer =
          this.manager.getPlayers().get(event.getEntity().getKiller().getUniqueId());
      if (killer == null) {
        return;
      }
      killer.setKills(killer.getKills() + 1);
    }
    this.manager.getPlugin().getServer().broadcastMessage(message); // TODO redo these messages
    this.manager.checkForWin();
  }
}
