package net.venade.games.crystalsg;

import net.minecraft.server.v1_16_R3.ChatMessage;
import net.minecraft.server.v1_16_R3.IChatBaseComponent;
import net.minecraft.server.v1_16_R3.LocaleLanguage;
import net.venade.games.crystalsg.manager.npc.Npc;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * @author JakeMT04
 * @since 17/08/2021
 */
public class MiscUtils {
  public static String millisToTimer(long millis) {
    long seconds = millis / 1000L;
    String HOUR_FORMAT = "%02d:%02d:%02d";
    String MINUTE_FORMAT = "%02d:%02d";
    if (seconds > 3600L) {
      return String.format(HOUR_FORMAT, seconds / 3600L, seconds % 3600L / 60L, seconds % 60L);
    } else {
      return String.format(MINUTE_FORMAT, seconds / 60L, seconds % 60L);
    }
  }

  public static boolean isInside(Player player, Location border, int size) {
    double minX = border.getX() - size;
    double maxX = border.getX() + size;
    double minZ = border.getZ() - size;
    double maxZ = border.getZ() + size;
    return !(player.getLocation().getX() < minX
        || player.getLocation().getX() > maxX
        || player.getLocation().getZ() < minZ
        || player.getLocation().getZ() > maxZ);
  }

  public static Location getCorrectedPosition(Location player, Location border, int size) {
    double minX = border.getX() - size;
    double maxX = border.getX() + size;
    double minZ = border.getZ() - size;
    double maxZ = border.getZ() + size;
    double x = player.getX();
    double y = player.getY();
    double z = player.getZ();
    double bX = border.getX();
    double bZ = border.getZ();
    if (Math.abs(x - bX) > Math.abs(z - bZ)) {
      if (x <= maxX || x >= minX) {
        if (x > bX) {
          x = maxX + 3;
        } else {
          x = minX - 3;
        }
      } else if (z <= maxZ || z >= minZ) {
        if (z > bZ) {
          z = maxZ + 3;
        } else {
          z = minZ - 3;
        }
      }
    } else {
      if (z <= maxZ || z <= minZ) {
        if (z > bZ) {
          z = maxZ + 3;
        } else {
          z = minZ - 3;
        }
      } else if (x <= maxX || x <= minX) {
        if (x > bX) {
          x = maxX + 3;
        } else {
          x = minX - 3;
        }
      }
    }

    int ixLoc = Location.locToBlock(x);
    int izLoc = Location.locToBlock(z);
    Chunk tChunk = player.getWorld().getChunkAt(blockToChunk(ixLoc), blockToChunk(izLoc));
    if (!tChunk.isLoaded()) tChunk.load();
    y = MiscUtils.getSafeY(player.getWorld(), ixLoc, Location.locToBlock(y), izLoc);
    if (y == -1) return null;
    return new Location(
        player.getWorld(),
        Math.floor(x) + 0.5,
        y,
        Math.floor(z) + 0.5,
        player.getYaw(),
        player.getPitch());
  }

  public static int blockToChunk(int blockVal) { // 1 chunk is 16x16 blocks
    return blockVal >> 4; // ">>4" == "/16"
  }

  public static String getOtherDeathMessage(Npc npc) {
    ChatMessage deathMessage =
        ((ChatMessage)
            ((CraftPlayer) npc.getEntity()).getHandle().getCombatTracker().getDeathMessage());
    String code = deathMessage.getKey();
    String text = LocaleLanguage.a().a(code);
    return "§f"
        + String.format(
            text,
            Arrays.stream(deathMessage.getArgs())
                .map(
                    o -> {
                      if (o instanceof IChatBaseComponent) {
                        StringBuilder builder = new StringBuilder();
                        (((IChatBaseComponent) o).getSiblings())
                            .forEach(a -> builder.append(a.getText()));
                        return ChatColor.stripColor(((IChatBaseComponent) o).getText() + builder);
                      }
                      return ChatColor.stripColor(o.toString());
                    })
                .map(
                    o -> {
                      if (npc.getIdentity().getName().equalsIgnoreCase(o)) {
                        return "§b" + npc.getEntity().getName() + " §7[Combat Log]§f";
                      }
                      Player player = Bukkit.getPlayer(o);
                      if (player != null) {
                        return "§b" + player.getName() + "§f";
                      }
                      return "§b" + o + "§f";
                    })
                .toArray());
  }

  public static String getOtherDeathMessage(Player player) {
    ChatMessage deathMessage =
        ((ChatMessage) ((CraftPlayer) player).getHandle().getCombatTracker().getDeathMessage());
    String code = deathMessage.getKey();
    String text = LocaleLanguage.a().a(code);
    return "§f"
        + String.format(
            text,
            Arrays.stream(deathMessage.getArgs())
                .map(
                    o -> {
                      if (o instanceof IChatBaseComponent) {
                        StringBuilder builder = new StringBuilder();
                        (((IChatBaseComponent) o).getSiblings())
                            .forEach(a -> builder.append(a.getText()));
                        return ChatColor.stripColor(((IChatBaseComponent) o).getText() + builder);
                      }
                      return ChatColor.stripColor(o.toString());
                    })
                .map(
                    o -> {
                      Player other = Bukkit.getPlayer(o);
                      if (other != null) {
                        return "§b" + other.getName() + "§f";
                      }
                      return "§b" + o + "§f";
                    })
                .toArray());
  }

  private static boolean isSafeSpot(World world, int X, int Y, int Z) {
    boolean safe =
        world.getBlockAt(X, Y, Z).getType() == Material.AIR // target block open and safe
            && world.getBlockAt(X, Y + 1, Z).getType()
                == Material.AIR; // above target block open and safe
    Material below = world.getBlockAt(X, Y - 1, Z).getType();
    // below target block not painful
    return safe
        && (below
            .isSolid()); // below target block not open/breathable (so presumably solid), or is wate
  }

  public static double getSafeY(World world, int x, int y, int z) {
    final boolean isNether = world.getEnvironment() == World.Environment.NETHER;
    int limTop = isNether ? 125 : world.getMaxHeight() - 2;
    final int highestBlockBoundary = Math.min(world.getHighestBlockYAt(x, z) + 1, limTop);
    if (y > limTop) {
      if (isNether) {
        y = limTop;
      } else {
        y = highestBlockBoundary; // there will never be a save block to stand on for Y values >
        // highestBlockBoundary
      }
    }
    int limBot = 0;
    if (y < limBot) {
      y = limBot;
    }
    if (!isNether) {
      limTop = highestBlockBoundary;
    }
    for (int y1 = y, y2 = y; (y1 > limBot) || (y2 < limTop); y1--, y2++) {
      // Look below.
      if (y1 > limBot) {
        if (isSafeSpot(world, x, y1, z)) return y1;
      }

      // Look above.
      if (y2 < limTop && y2 != y1) {
        if (isSafeSpot(world, x, y2, z)) return y2;
      }
    }
    return -1.0D;
  }
}
