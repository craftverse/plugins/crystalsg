package net.venade.games.crystalsg.cmd;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.cmd.test.FastForward;
import net.venade.games.crystalsg.cmd.test.PopulateChest;
import net.venade.games.crystalsg.cmd.test.SpawnCombatLogEntity;
import net.venade.games.crystalsg.cmd.test.SpawnFeastCommand;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 18/08/2021
 */
public class TestSubCommand extends VenadeCommand {
  public TestSubCommand(CrystalSGPlugin plugin) {
    super("test", "Test command", null, true);
    registerSubCommand(new SpawnFeastCommand(plugin.getManager()));
    registerSubCommand(new FastForward(plugin.getManager()));
    registerSubCommand(new PopulateChest(plugin.getManager()));
    registerSubCommand(new SpawnCombatLogEntity(plugin.getManager()));
  }
}
