package net.venade.games.crystalsg.cmd.loot;

/**
 * @author JakeMT04
 * @since 17/08/2021
 */
public enum ItemLocation {
  LOOT,
  FEAST,
  BOTH
}
