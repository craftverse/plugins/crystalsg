package net.venade.games.crystalsg.cmd.test;

import net.venade.games.crystalsg.manager.GameManager;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;

/**
 * @author JakeMT04
 * @since 17/08/2021
 */
public class SpawnFeastCommand extends VenadeCommand {

  private final GameManager manager;

  public SpawnFeastCommand(GameManager manager) {
    super("spawnfeast", "Spawns the feast", null);
    this.manager = manager;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    manager.getLootGenerator().populateFeast();
    sender.sendRawMessage("Attempted to spawn the feast.");
  }
}
