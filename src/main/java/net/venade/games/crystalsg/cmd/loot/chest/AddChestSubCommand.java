package net.venade.games.crystalsg.cmd.loot.chest;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author JakeMT04
 * @since 05/08/2021
 */
public class AddChestSubCommand extends VenadeCommand {
  static final AtomicBoolean BLOCKED = new AtomicBoolean(false);
  private final CrystalSGPlugin plugin;

  public AddChestSubCommand(CrystalSGPlugin plugin) {
    super("add", "crystal.command.loot.chest.add", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onExecute(Sender sender) {
    if (sender.isConsole()) {
      return;
    }
    if (BLOCKED.get()) {
      sender.sendRawMessage(
          "§cAn auto-add operation is in progress. Please wait for it to finish.");
      return;
    }
    Player player = sender.getAsPlayer();
    List<Block> sightLine =
        player.getLineOfSight(EnumSet.of(Material.AIR, Material.CAVE_AIR, Material.VOID_AIR), 10);
    Optional<Block> optionalBlock =
        sightLine.stream().filter(i -> i.getType() != Material.AIR).findFirst();
    if (optionalBlock.isEmpty()) {
      sender.sendRawMessage(
          "§cYou are not looking at a block. (" + sightLine.size() + " blocks scanned)");
      return;
    }
    Block block = optionalBlock.get();
    int x = block.getX();
    int y = block.getY();
    int z = block.getZ();
    List<String> section =
        new ArrayList<>(
            this.plugin.getLootConfig().getStringList("chests." + block.getWorld().getName()));
    String s = x + ";" + y + ";" + z;
    if (section.contains(s)) {
      sender.sendRawMessage("§cThis location is already registered.");
      return;
    }
    section.add(s);
    this.plugin.getLootConfig().set("chests." + block.getWorld().getName(), section);
    this.plugin.saveLootConfig();
    sender.sendRawMessage(
        "You have registered the block at §b"
            + x
            + ", "
            + y
            + ", "
            + z
            + "§f to be a loot container.");
  }
}
