package net.venade.games.crystalsg.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.maps.MapAPI;
import net.venade.maps.model.DefaultMap;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * @author Nikolas Rummel
 * @since 25.08.2021
 */
public class MapVotingInventory implements InventoryProvider {

  private final SmartInventory inventory;
  private final GameManager manager;

  private final HashMap<DefaultMap, List<UUID>> votes;

  public MapVotingInventory(GameManager manager) {
    this.manager = manager;
    this.inventory =
        SmartInventory.builder()
            .manager(VenadeAPI.getInventoryManager())
            .id("map_voting")
            .provider(this)
            .size(3, 9)
            .title("§8Maps")
            .build();

    this.votes = new HashMap<>();
    MapAPI.getInstance().getLoadedMaps().forEach(map -> votes.put(map, new ArrayList<>()));
  }

  @Override
  public void init(Player player, InventoryContents contents) {
    player.playSound(player.getLocation(), Sound.ENTITY_ENDER_EYE_DEATH, 1F, 1F);
    fillInventory(player, contents);
  }

  @Override
  public void update(Player player, InventoryContents contents) {
    fillInventory(player, contents);
  }

  private void fillInventory(Player player, InventoryContents contents) {
    int count = 0;
    for (DefaultMap map : MapAPI.getInstance().getLoadedMaps()) {
      contents.set(
          1,
          count,
          ClickableItem.of(
              ItemBuilder.builder(Material.valueOf(map.getMaterial()))
                  .display("§b§l" + map.getName())
                  .lore("§7Builder: §f" + map.getBuilder(), "§7Votes: §f" + map.getVotes())
                  .build(),
              event -> handleClick(map, player)));
      count++;
    }
  }

  private void handleClick(DefaultMap clickedMap, Player player) {
    CrystalSGPlayer csgPlayer = this.manager.getPlayers().get(player.getUniqueId());

    if (!csgPlayer.getVotedMap().equalsIgnoreCase("")) {
      Optional<DefaultMap> optional =
          votes.keySet().stream()
              .filter(map -> map.getName().equals(csgPlayer.getVotedMap()))
              .findFirst();
      if (optional.isPresent()) {
        if (clickedMap.getName().equals(optional.get().getName())) {
          player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1F, 1F);
          return;
        }
        optional.get().setVotes(optional.get().getVotes() - 1);
        votes.get(optional.get()).remove(player.getUniqueId());
      }
    }

    csgPlayer.setVotedMap(clickedMap.getName());
    votes.get(clickedMap).add(csgPlayer.getBase().getUniqueId());

    Optional<DefaultMap> optional =
        votes.keySet().stream()
            .filter(map -> clickedMap.getName().equals(csgPlayer.getVotedMap()))
            .findFirst();

    optional.ifPresent(map -> clickedMap.setVotes(clickedMap.getVotes() + 1));

    player.playSound(player.getLocation(), Sound.UI_CARTOGRAPHY_TABLE_TAKE_RESULT, 1F, 1F);
    player.getOpenInventory().close();
  }

  public void removePlayer(CrystalSGPlayer player) {
    if (!player.getVotedMap().equalsIgnoreCase("")) {
      Optional<DefaultMap> optional =
          votes.keySet().stream()
              .filter(map -> map.getName().equals(player.getVotedMap()))
              .findFirst();

      optional.ifPresent(map -> map.setVotes(map.getVotes() - 1));
      optional.ifPresent(map -> votes.get(map).remove(player.getBase().getUniqueId()));
      player.setVotedMap("");
    }
  }

  public DefaultMap getVotedMap() {
    return votes.entrySet().stream()
        .max((entry1, entry2) -> entry1.getKey().getVotes() > entry2.getKey().getVotes() ? 1 : -1)
        .orElseThrow()
        .getKey();
  }

  public void open(Player player) {
    this.inventory.open(player);
  }
}
