package net.venade.games.crystalsg.cmd.loot.chest;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

/**
 * @author JakeMT04
 * @since 05/08/2021
 */
public class RemoveChestSubCommand extends VenadeCommand {
  private final CrystalSGPlugin plugin;

  public RemoveChestSubCommand(CrystalSGPlugin plugin) {
    super("remove", "crystal.command.loot.chest.remove", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onExecute(Sender sender) {
    if (sender.isConsole()) {
      return;
    }
    Player player = sender.getAsPlayer();
    List<Block> sightLine =
        player.getLineOfSight(EnumSet.of(Material.AIR, Material.CAVE_AIR, Material.VOID_AIR), 10);
    Optional<Block> optionalBlock =
        sightLine.stream().filter(i -> i.getType() != Material.AIR).findFirst();
    if (optionalBlock.isEmpty()) {
      sender.sendRawMessage(
          "§cYou are not looking at a block. (" + sightLine.size() + " blocks scanned)");
      return;
    }
    Block block = optionalBlock.get();
    int x = block.getX();
    int y = block.getY();
    int z = block.getZ();
    List<String> section =
        new ArrayList<>(
            this.plugin.getLootConfig().getStringList("chests." + block.getWorld().getName()));
    String s = x + ";" + y + ";" + z;
    if (section.contains(s)) {
      section.remove(s);
      this.plugin.getLootConfig().set("chests." + block.getWorld().getName(), section);
      this.plugin.saveLootConfig();
      sender.sendRawMessage(
          "You have removed the block at §b"
              + x
              + ", "
              + y
              + ", "
              + z
              + "§f from the loot container list.");
      return;
    }
    sender.sendRawMessage("§cThis location is not registered.");
  }
}
