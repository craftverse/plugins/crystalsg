package net.venade.games.crystalsg.manager.npc;

import net.venade.games.crystalsg.CrystalSGPlugin;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class NpcManager {
  private static final Map<UUID, Npc> spawnedNpcs = new HashMap<>();

  private static final Map<Npc, NpcDespawnTask> despawnTasks = new HashMap<>();

  public static Npc spawn(Player player) {
    // Do nothing if player already has a NPC
    Npc npc = getSpawnedNpc(player.getUniqueId());
    if (npc != null) return null;

    // Spawn fake player entity
    npc = new Npc(NpcEntitySpawner.spawn(player));
    spawnedNpcs.put(player.getUniqueId(), npc);

    Player entity = npc.getEntity();

    entity.setCanPickupItems(false);
    entity.setNoDamageTicks(0);

    // Copy player data to fake player
    entity.setHealthScale(player.getHealthScale());
    //noinspection ConstantConditions
    entity
        .getAttribute(Attribute.GENERIC_MAX_HEALTH)
        .setBaseValue(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
    entity.setHealth(player.getHealth());
    entity.setTotalExperience(player.getTotalExperience());
    entity.setFoodLevel(player.getFoodLevel());
    entity.setExhaustion(player.getExhaustion());
    entity.setSaturation(player.getSaturation());
    entity.setFireTicks(player.getFireTicks());
    entity.getInventory().setContents(player.getInventory().getContents());
    entity.getInventory().setArmorContents(player.getInventory().getArmorContents());
    entity.addPotionEffects(player.getActivePotionEffects());
    entity.setDisplayName(player.getDisplayName());

    // Should fix some visual glitches, such as health bars displaying zero
    // TODO: Find another solution. This one causes the player to be added to the NMS PlayerList,
    // that's not ideal.
    entity.teleport(player.getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
    // Send equipment packets to nearby players
    NpcEntitySpawner.updateEquipment(entity);

    entity.setMetadata(
        "NPC", new FixedMetadataValue(CrystalSGPlugin.getPlugin(CrystalSGPlugin.class), true));

    // Create and start the NPCs despawn task
    long despawnTime =
        System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(2) + TimeUnit.SECONDS.toMillis(30);
    NpcDespawnTask despawnTask =
        new NpcDespawnTask(CrystalSGPlugin.getPlugin(CrystalSGPlugin.class), npc, despawnTime);
    despawnTask.start();
    despawnTasks.put(npc, despawnTask);
    return npc;
  }

  public static void despawn(Npc npc) {
    // Do nothing if NPC isn't spawned or if it's a different NPC
    Npc other = getSpawnedNpc(npc.getIdentity().getId());
    if (other == null || other != npc) return;

    // Cancel the NPCs despawn task if they have one
    if (hasDespawnTask(npc)) {
      NpcDespawnTask despawnTask = getDespawnTask(npc);
      despawnTask.stop();
      despawnTasks.remove(npc);
    }

    // Remove the NPC entity from the world
    NpcEntitySpawner.despawn(npc.getEntity());
    spawnedNpcs.remove(npc.getIdentity().getId());
    npc.getEntity().removeMetadata("NPC", CrystalSGPlugin.getPlugin(CrystalSGPlugin.class));
  }

  private static double getRealMaxHealth(Player npcPlayer) {
    double health = npcPlayer.getMaxHealth();
    for (PotionEffect p : npcPlayer.getActivePotionEffects()) {
      if (p.getType().equals(PotionEffectType.HEALTH_BOOST)) {
        health -= (p.getAmplifier() + 1) * 4;
      }
    }
    return health;
  }

  public static void playerRejoinHandler(Player player, Npc npc) {
    // Save player data when the NPC despawns
    // Copy NPC player data to online player
    Player npcPlayer = npc.getEntity();
    player.setMaximumAir(npcPlayer.getMaximumAir());
    player.setRemainingAir(npcPlayer.getRemainingAir());
    player.setHealthScale(npcPlayer.getHealthScale());
    player.setMaxHealth(getRealMaxHealth(npcPlayer));
    player.setHealth(npcPlayer.getHealth());
    player.setTotalExperience(npcPlayer.getTotalExperience());
    player.setFoodLevel(npcPlayer.getFoodLevel());
    player.setExhaustion(npcPlayer.getExhaustion());
    player.setSaturation(npcPlayer.getSaturation());
    player.setFireTicks(npcPlayer.getFireTicks());
    player.getInventory().setContents(npcPlayer.getInventory().getContents());
    player.getInventory().setArmorContents(npcPlayer.getInventory().getArmorContents());
    player.addPotionEffects(npcPlayer.getActivePotionEffects());
  }

  public static Npc getSpawnedNpc(UUID playerId) {
    return spawnedNpcs.get(playerId);
  }

  public static boolean npcExists(UUID playerId) {
    return spawnedNpcs.containsKey(playerId);
  }

  public static NpcDespawnTask getDespawnTask(Npc npc) {
    return despawnTasks.get(npc);
  }

  public static boolean hasDespawnTask(Npc npc) {
    return despawnTasks.containsKey(npc);
  }

  public static void shutdown() {
    // despawn all NPC's
    for (Map.Entry<UUID, Npc> npc : spawnedNpcs.entrySet()) {
      despawn(npc.getValue());
    }
  }

  public static Collection<Npc> getSpawnedNpcs() {
    return spawnedNpcs.values();
  }
}
