package net.venade.games.crystalsg;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.service.group.CloudServiceGroupType;
import cloud.cubid.module.service.group.ICloudServiceGroup;
import cloud.cubid.module.service.service.ICloudService;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.Getter;
import net.venade.games.crystalsg.cmd.AdminCommand;
import net.venade.games.crystalsg.cmd.loot.ItemLocation;
import net.venade.games.crystalsg.listener.PlayerInteractListener;
import net.venade.games.crystalsg.listener.PlayerJoinListener;
import net.venade.games.crystalsg.listener.PlayerMovementListener;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.manager.npc.NpcManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.exc.CommandArgumentException;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Provider;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.command.registration.CommandManager;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author JakeMT04
 * @since 01/08/2021
 */
@Getter
public class CrystalSGPlugin extends JavaPlugin {
  private GameManager manager;
  private FileConfiguration lootConfig;
  private File lootFile;

  @Override
  public void onEnable() {
    this.saveDefaultConfig();
    this.saveResource("loot.yml", false);
    this.manager = new GameManager(this);
    this.lootFile = new File(this.getDataFolder(), "loot.yml");
    this.lootConfig = YamlConfiguration.loadConfiguration(this.lootFile);
    this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(this.manager), this);
    this.getServer()
        .getPluginManager()
        .registerEvents(new PlayerMovementListener(this.manager), this);
    this.getServer()
        .getPluginManager()
        .registerEvents(new PlayerInteractListener(this.manager), this);
    Providers.bind(
        CrystalSGPlayer.class,
        new Provider<>() {
          @Override
          public CrystalSGPlayer provide(Context ctx) throws CommandException {
            Player player = Providers.provider(Player.class).provide(ctx);
            return manager.getPlayers().get(player.getUniqueId());
          }

          @Override
          public List<String> suggest(Context ctx) {
            return Providers.provider(Player.class).suggest(ctx);
          }
        });
    Providers.bind(
        ItemLocation.class,
        new Provider<>() {
          @Override
          public ItemLocation provide(Context context) throws CommandException {
            try {
              return ItemLocation.valueOf(context.getArgument().toUpperCase());
            } catch (IllegalArgumentException e) {
              String accepted =
                  Arrays.stream(ItemLocation.values())
                      .map(l -> "§f" + l.name().toLowerCase())
                      .collect(Collectors.joining("§c, "));
              throw new CommandArgumentException(
                  true, "§cLocation must be one of: " + accepted + "§c.");
            }
          }

          @Override
          public List<String> suggest(Context context) {
            return Arrays.stream(ItemLocation.values())
                .map(l -> l.name().toLowerCase())
                .sorted()
                .collect(Collectors.toList());
          }
        });
    this.getServer()
        .getScheduler()
        .runTaskTimerAsynchronously(
            this,
            () -> {
              for (CrystalSGPlayer player : this.manager.getPlayers().values()) {
                this.manager.getScoreboardManager().updateScoreboard(player);
              }
            },
            10L,
            10L);
    World world = this.getServer().getWorld("WaitingHub");
    if (world != null) {
      world.setGameRule(GameRule.FALL_DAMAGE, false);
      world.setGameRule(GameRule.DROWNING_DAMAGE, false);
      world.setGameRule(GameRule.FIRE_DAMAGE, false);
      world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
      world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
    }
    CommandManager.registerCommand(new AdminCommand(this), this);
  }

  @Override
  public void onDisable() {
    NpcManager.shutdown();
  }

  public void saveLootConfig() {
    try {
      lootConfig.save(this.lootFile);
    } catch (IOException var2) {
      this.getLogger().log(Level.SEVERE, "Could not save config to " + this.lootFile, var2);
    }
  }

  public void broadcast(String key, Object... args) {
    for (CrystalSGPlayer player : this.manager.getPlayers().values()) {
      player.getCorePlayer().sendMessage(key, args);
    }
  }

  public void reloadLootConfig() {
    this.lootConfig = YamlConfiguration.loadConfiguration(this.lootFile);
  }

  public void sendToHub(final Player player) {
    CubidCloud.getBridge()
        .getPlayerManager()
        .getOnlinePlayer(player.getUniqueId())
        .ifPresent(
            cubidPlayer -> {
              final String serviceName = cubidPlayer.getConnectedService();
              if (serviceName == null) return;

              final String groupName = serviceName.split("-")[0];
              final ICloudServiceGroup group =
                  CubidCloud.getBridge().getServiceManager().getCloudGroup(groupName).orElse(null);

              if (group != null
                  && (group.getServerGroupType() == CloudServiceGroupType.LOBBY
                      || group.getServerGroupType() == CloudServiceGroupType.STATIC_LOBBY)) {
                cubidPlayer.sendLanguageMessage("cubid.message.hub.already");
                return;
              }

              final ICloudService hub =
                  CubidCloud.getBridge().getServiceManager().findFallbackService().orElse(null);
              if (hub == null) {
                cubidPlayer.sendLanguageMessage("cubid.message.hub.error");
                return;
              }

              cubidPlayer.connect(hub);
            });
  }
}
