package net.venade.games.crystalsg.task;

import lombok.Getter;
import lombok.Setter;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author JakeMT04
 * @since 16/08/2021
 */
@Setter
@Getter
public class CountdownTask extends BukkitRunnable {
  private final GameManager manager;
  private final String broadcastMessage;
  private final Runnable onComplete;
  private final boolean sound;
  private long endTime;
  private boolean finished = false;
  private long lastBroadcasted = -1;

  public CountdownTask(
      GameManager manager,
      long endTime,
      boolean sound,
      String broadcastMessage,
      Runnable onComplete) {
    this.manager = manager;
    this.endTime = endTime;
    this.broadcastMessage = broadcastMessage;
    this.sound = sound;
    this.onComplete = onComplete;
  }

  @Override
  public void run() {
    if (finished) {
      return;
    }
    long diff = this.endTime - System.currentTimeMillis();
    long diffSecs = diff / 1000;
    if (diffSecs <= 0) {
      this.onComplete.run();
      this.finished = true;
      this.cancel();
      if (this.sound) {
        for (CrystalSGPlayer player : this.manager.getPlayers().values()) {
          player
              .getBase()
              .playSound(player.getBase().getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
        }
      }
      return;
    }
    if (this.shouldBroadcast(diffSecs)) {
      this.lastBroadcasted = diffSecs;
      this.manager.broadcast(this.broadcastMessage, MiscUtils.millisToTimer(diff));
      if (this.sound && diffSecs <= 60) {
        for (CrystalSGPlayer player : this.manager.getPlayers().values()) {
          player
              .getBase()
              .playSound(player.getBase().getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
        }
      }
    }
  }

  public boolean shouldBroadcast(long diff) {
    if (diff == lastBroadcasted) {
      return false;
    }
    if (diff % 60 == 0) {
      return true;
    }
    if (diff <= 5 || diff == 15 || diff == 10) {
      return true;
    } else if (diff <= 60) {
      return diff % 15 == 0;
    } else if (diff <= 120) {
      return diff % 30 == 0;
    }
    return false;
  }
  //
  //    private void decrementBroadcastNext() {
  //        if (broadcastNext == -1) {
  //            return;
  //        }
  //        if (broadcastNext == 0) {
  //            broadcastNext = -1;
  //            onComplete.run();
  //            return;
  //        }
  //        if (broadcastNext <= 10) {
  //            broadcastNext -= 1;
  //        } else if (broadcastNext == 15) {
  //            broadcastNext = 10;
  //        } else if (broadcastNext <= 60) {
  //            broadcastNext -= 15;
  //        } else if (broadcastNext <= 120) {
  //            broadcastNext -= 30;
  //        } else {
  //            broadcastNext -= 60;
  //        }
  //    }
}
