package net.venade.games.crystalsg.cmd.test;

import net.venade.games.crystalsg.manager.GameManager;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Default;

/**
 * @author JakeMT04
 * @since 16/08/2021
 */
public class FastForward extends VenadeCommand {
  private final GameManager manager;

  public FastForward(GameManager manager) {
    super("fastforward", "Fast forwards the game", null);
    this.manager = manager;
  }

  @CommandMethod
  public void onCommand(Sender sender, @Default("5") int seconds) {
    long pushBack = seconds * 1000L;
    manager.setStartedAt(manager.getStartedAt() - pushBack);
    manager.getPvpEnableTask().setEndTime(manager.getPvpEnableTask().getEndTime() - pushBack);
    manager.getFeastSpawnTask().setEndTime(manager.getFeastSpawnTask().getEndTime() - pushBack);
    manager.getDeathmatchTask().setEndTime(manager.getDeathmatchTask().getEndTime() - pushBack);

    sender.sendRawMessage("Fast-forwarded the game by §b" + seconds + " seconds§f.");
  }
}
