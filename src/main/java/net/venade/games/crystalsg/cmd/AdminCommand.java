package net.venade.games.crystalsg.cmd;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.cmd.loot.LootSubCommand;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.permission.Permission;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class AdminCommand extends VenadeCommand {
  public AdminCommand(CrystalSGPlugin plugin) {
    super("admin", "", Permission.has(CubidRank.DEV, CubidRank.SR_DEV, CubidRank.ADMIN), true);
    // registerSubCommand(new LocationSubCommand(plugin));
    registerSubCommand(new LootSubCommand(plugin));
    registerSubCommand(new ReloadConfiguration(plugin));
    registerSubCommand(new StartGameRightNow(plugin.getManager()));
    registerSubCommand(new TestSubCommand(plugin));
  }
}
