package net.venade.games.crystalsg.cmd.loot.chest;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 05/08/2021
 */
public class ChestSubCommand extends VenadeCommand {
  public ChestSubCommand(CrystalSGPlugin plugin) {
    super("chest", "crystal.command.loot.chest", null, true, "container");
    registerSubCommand(new AddChestSubCommand(plugin));
    registerSubCommand(new RemoveChestSubCommand(plugin));
    // registerSubCommand(new AutoAddChestsCommand(plugin));

  }
}
