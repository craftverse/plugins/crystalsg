package net.venade.games.crystalsg.manager.npc;

import org.bukkit.entity.Player;

public class Npc {
  private final NpcIdentity identity;

  private final Player entity;

  Npc(Player entity) {
    this.identity = NpcEntitySpawner.getIdentity(entity);
    this.entity = entity;
  }

  public NpcIdentity getIdentity() {
    return identity;
  }

  public Player getEntity() {
    return entity;
  }
}
