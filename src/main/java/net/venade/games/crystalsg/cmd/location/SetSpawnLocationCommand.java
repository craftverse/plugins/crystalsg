package net.venade.games.crystalsg.cmd.location;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class SetSpawnLocationCommand extends VenadeCommand {
  private final CrystalSGPlugin plugin;

  public SetSpawnLocationCommand(CrystalSGPlugin plugin) {
    super("spawn", "crystal.command.location.spawn", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    if (sender.isConsole()) return;
    Player player = sender.getAsPlayer();
    Location location = player.getLocation();
    plugin.getConfig().set(location.getWorld().getName() + ".initial-spawn", location);
    plugin.saveConfig();
    sender.sendMessage(
        "Successfully set the spawn location for map &b"
            + location.getWorld().getName()
            + "&f to your current location.");
  }
}
