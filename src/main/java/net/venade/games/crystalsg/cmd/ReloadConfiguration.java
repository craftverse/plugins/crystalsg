package net.venade.games.crystalsg.cmd;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class ReloadConfiguration extends VenadeCommand {
  private final CrystalSGPlugin plugin;

  public ReloadConfiguration(CrystalSGPlugin plugin) {
    super("reload", "crystal.command.reload", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    this.plugin.reloadConfig();
    this.plugin.reloadLootConfig();
    sender.sendRawMessage("Configuration files reloaded! §7[config.yml, loot.yml]");
  }
}
