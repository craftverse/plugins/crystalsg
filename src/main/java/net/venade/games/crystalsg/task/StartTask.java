package net.venade.games.crystalsg.task;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.maps.MapAPI;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@RequiredArgsConstructor
@Setter
public class StartTask extends BukkitRunnable {
  private final GameManager manager;
  private long broadcastNext = 60;
  private boolean hasPickedMap = false;

  @Override
  public void run() {
    if (broadcastNext == -1) {
      return;
    }
    if (manager.getTimeToStart() != 0) {
      long diff = manager.getTimeToStart() - System.currentTimeMillis();
      long diffSecs = diff / 1000;
      if (diffSecs <= 0 && !this.hasPickedMap) {
        MapAPI.setPlayMap(this.manager.getMapVotingInventory().getVotedMap());
        this.hasPickedMap = true;
        this.manager.teleportIntoWorld();
        for (CrystalSGPlayer player : manager.getPlayers().values()) {
          player
              .getBase()
              .sendTitle(
                  "§b" + MapAPI.getPlayMap().getName(),
                  player
                      .getCorePlayer()
                      .getLanguageString("crystal.map.voted", MapAPI.getPlayMap().getBuilder()),
                  20,
                  20,
                  20);
        }
        return;
      }
      if (diffSecs == broadcastNext) {
        String startMessage =
            manager.getState() == GameManager.GameState.STARTING
                ? "crystal.game.release"
                : "crystal.game.start";
        manager.broadcast(startMessage, MiscUtils.millisToTimer(diff));
        setBroadcastNext();

        for (CrystalSGPlayer player : manager.getPlayers().values()) {
          player
              .getBase()
              .playSound(
                  player.getBase().getLocation(),
                  Sound.BLOCK_NOTE_BLOCK_PLING,
                  1,
                  diffSecs == 0 ? 2 : 1);
        }
      }
    }
  }

  private void setBroadcastNext() {
    if (broadcastNext == -1) {
      return;
    }
    if (broadcastNext == 0) {
      broadcastNext = -1;
      manager.startGame();
      return;
    }
    if (broadcastNext <= 5) {
      broadcastNext -= 1;
    } else if (broadcastNext == 10) {
      broadcastNext = 5;
    } else if (broadcastNext == 15) {
      broadcastNext = 10;
    } else {
      broadcastNext -= 15;
    }
  }
}
