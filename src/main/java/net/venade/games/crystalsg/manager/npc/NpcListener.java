package net.venade.games.crystalsg.manager.npc;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.UUID;

public class NpcListener implements Listener {
  private final CrystalSGPlugin plugin;

  public NpcListener(CrystalSGPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    NpcEntitySpawner.createPlayerList(event.getPlayer());
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void despawnNpc(PlayerDeathEvent event) {
    // Do nothing if player is not a NPC
    if (event.getDeathMessage() == null) return;

    Player player = event.getEntity();
    if (!NpcEntitySpawner.isNpc(player)) return;

    // Fetch NPC using the actual player identity
    UUID id = NpcEntitySpawner.getIdentity(player).getId();
    final Npc npc = NpcManager.getSpawnedNpc(id);
    if (npc == null) return;
    CrystalSGPlayer csgPlayer = this.plugin.getManager().getPlayers().get(id);
    if (csgPlayer != null) {
      csgPlayer.setState(CrystalSGPlayer.PlayerState.LOGGED_DEAD);
      player.getWorld().strikeLightningEffect(player.getLocation());
      csgPlayer.saveInventory(npc.getEntity());
      String message;
      if (event.getDeathMessage().equalsIgnoreCase("hahachangeme")) {
        message =
            "§b"
                + npc.getIdentity().getName()
                + " §7[§bCombat Logger§7] §fwas logged out for too long";
      } else {
        message = MiscUtils.getOtherDeathMessage(npc);
      }
      message += "§f.";
      plugin.getServer().broadcastMessage(message);
    }
    event.setDeathMessage(null);

    // Despawn NPC on the next tick
    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> NpcManager.despawn(npc));
  }
}
