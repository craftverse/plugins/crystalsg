package net.venade.games.crystalsg.listener;

import lombok.AllArgsConstructor;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.manager.npc.Npc;
import net.venade.games.crystalsg.manager.npc.NpcManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@AllArgsConstructor
public class PlayerJoinListener implements Listener {
  private final GameManager manager;

  @SuppressWarnings("ConstantConditions")
  @EventHandler(priority = EventPriority.LOW)
  public void onJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    event.setJoinMessage(null);

    CrystalSGPlayer csgPlayer;
    if (this.manager.getState().isStarted()) {
      // check if they are already registered
      csgPlayer = this.manager.getPlayers().get(player.getUniqueId());
      if (csgPlayer != null) {
        csgPlayer.setBase(player);
        if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.LOGGED_ALIVE) {
          Npc npc = NpcManager.getSpawnedNpc(player.getUniqueId());
          if (npc != null) {
            NpcManager.playerRejoinHandler(player, npc);
            NpcManager.despawn(npc);
          }
          csgPlayer.setState(CrystalSGPlayer.PlayerState.PLAYING);
          this.manager.getVisibilityManager().processPlayer(csgPlayer);
        } else if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.LOGGED_DEAD) {
          csgPlayer.setState(CrystalSGPlayer.PlayerState.DEAD);
          csgPlayer.setScoreBuilder(new VenadeBoard(player));
          this.manager.applySpectatorSettings(csgPlayer, "crystal.spec.reason.died-offline");
        } else if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.LOGGED_DIED) {
          csgPlayer.setState(CrystalSGPlayer.PlayerState.DEAD);
          csgPlayer.setScoreBuilder(new VenadeBoard(player));
          this.manager.applySpectatorSettings(csgPlayer, "crystal.spec.reason.died");
        }
      } else {
        csgPlayer = new CrystalSGPlayer(player);
        csgPlayer.setState(CrystalSGPlayer.PlayerState.SPEC);
        this.manager.registerPlayer(csgPlayer);
        this.manager.applySpectatorSettings(csgPlayer, "crystal.spec.reason.started");
      }
    } else {
      csgPlayer = new CrystalSGPlayer(player);
      csgPlayer.setState(CrystalSGPlayer.PlayerState.PLAYING);
      this.manager.registerPlayer(csgPlayer);
      String name =
          csgPlayer.getCorePlayer().getCubidPlayer().getRank().getColor()
              + player.getPlayer().getName();
      this.manager.broadcast(
          "crystal.joined", name, this.manager.getTotalPlayerCount(), this.manager.getMaxPlayers());
    }
    this.manager.getScoreboardManager().updateScoreboard(csgPlayer);
    // TODO Find another way to do no-collisions. This code breaks nametags
    //
    // this.manager.getPlugin().getServer().getScheduler().runTaskLater(this.manager.getPlugin(), ()
    // -> {
    //            Team team = event.getPlayer().getScoreboard().getTeam("nocollisions");
    //            if (team == null) {
    //                team = event.getPlayer().getScoreboard().registerNewTeam("nocollisions");
    //            }
    //            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
    //            team.addEntry(event.getPlayer().getName());
    //        }, 5L);
  }

  @EventHandler
  public void onLeave(PlayerQuitEvent event) {
    event.setQuitMessage(null);
    CrystalSGPlayer csgPlayer = this.manager.getPlayers().get(event.getPlayer().getUniqueId());
    if (csgPlayer == null) {
      return;
    }
    if (this.manager.getState().isStarted()) {

      if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.SPEC) {
        this.manager.unregisterPlayer(event.getPlayer());
        return;
      }
      if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.DEAD) {
        csgPlayer.setState(CrystalSGPlayer.PlayerState.LOGGED_DIED);
        // don't spawn an NPC
      } else {
        csgPlayer.setState(CrystalSGPlayer.PlayerState.LOGGED_ALIVE);
        NpcManager.spawn(event.getPlayer());
      }
    } else {
      Player player = event.getPlayer();

      this.manager.unregisterPlayer(player);
      String name =
          csgPlayer.getCorePlayer().getCubidPlayer().getRank().getColor() + player.getName();
      this.manager.broadcast(
          "crystal.quit", name, this.manager.getTotalPlayerCount(), this.manager.getMaxPlayers());
    }
  }
}
