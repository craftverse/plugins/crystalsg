package net.venade.games.crystalsg.manager.npc;

import net.venade.games.crystalsg.CrystalSGPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NpcDespawnTask implements Runnable {
  private final CrystalSGPlugin plugin;

  private final Npc npc;

  private long time;

  private int taskId;

  public NpcDespawnTask(CrystalSGPlugin plugin, Npc npc, long time) {
    this.plugin = plugin;
    this.npc = npc;
    this.time = time;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public Npc getNpc() {
    return npc;
  }

  public void start() {
    taskId = plugin.getServer().getScheduler().runTaskTimer(plugin, this, 1, 1).getTaskId();
  }

  public void stop() {
    plugin.getServer().getScheduler().cancelTask(taskId);
  }

  @Override
  public void run() {
    // Do nothing if NPC should not despawn yet
    if (time > System.currentTimeMillis()) {
      return;
    }
    List<ItemStack> items = new ArrayList<>();
    items.addAll(new ArrayList<>(Arrays.asList(npc.getEntity().getInventory().getContents())));
    items.addAll(new ArrayList<>(Arrays.asList(npc.getEntity().getInventory().getArmorContents())));
    PlayerDeathEvent event =
        new PlayerDeathEvent(
            npc.getEntity(),
            items,
            ((CraftPlayer) npc.getEntity()).getHandle().getExpReward(),
            "hahachangeme");
    Bukkit.getServer().getPluginManager().callEvent(event);
    for (ItemStack stack : event.getDrops()) {
      if (stack != null && stack.getType() != Material.AIR) {
        npc.getEntity()
            .getLocation()
            .getWorld()
            .dropItemNaturally(npc.getEntity().getLocation(), stack);
      }
    }
    NpcManager.despawn(npc);
  }
}
