package net.venade.games.crystalsg.task;

import lombok.RequiredArgsConstructor;
import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author JakeMT04
 * @since 16/08/2021
 */
@RequiredArgsConstructor
public class BorderShrinkTask extends BukkitRunnable {
  private final CrystalSGPlugin plugin;
  private final GameManager manager;
  private long lastShrunk = -1;
  private long lastBroadcasted = -1;

  public long getTimeToNextShrink(long now) {
    if (manager.getStartedAt() == 0) {
      return 0;
    }

    int size = (int) (this.manager.getActiveWorld().getWorldBorder().getSize() / 2);
    if (size <= 100) {
      return 0;
    }
    long shrinksStart = manager.getStartedAt() + 601_000;
    long diff = now - shrinksStart;
    if (diff < 0) {
      return 0;
    }
    long next = shrinksStart + 60_000 * ((diff / 60_000L) + 1);
    return (next - now) / 1000;
  }

  @Override
  public void run() {
    long now = System.currentTimeMillis();
    if (manager.getStartedAt() == 0) {
      return;
    }
    int size = (int) (this.manager.getActiveWorld().getWorldBorder().getSize() / 2);
    if (size <= 100) {
      return;
    }
    long shrinksStart = manager.getStartedAt() + 601_000;
    long ddif = now - shrinksStart;
    if (ddif < 0) {
      return;
    }
    if (now / 1000 == lastShrunk) {
      return;
    }
    long diff = this.getTimeToNextShrink(now);

    if (diff <= 0) {
      this.lastShrunk = now / 1000;
      this.plugin
          .getServer()
          .getScheduler()
          .runTask(
              this.plugin,
              () ->
                  this.manager
                      .getActiveWorld()
                      .getWorldBorder()
                      .setSize(this.manager.getActiveWorld().getWorldBorder().getSize() - 100));
    } else if (this.shouldBroadcast(diff)) {
      this.lastBroadcasted = diff;
      size -= 50;
      this.manager.broadcast("crystal.border.shrinkin", size, MiscUtils.millisToTimer(diff * 1000));
    }
  }

  public boolean shouldBroadcast(long diff) {
    if (diff == lastBroadcasted) {
      return false;
    }
    if (diff % 60 == 0) {
      return true;
    }
    if (diff <= 5 || diff == 10 || diff == 15) {
      return true;
    } else if (diff <= 60) {
      return diff % 15 == 0;
    } else if (diff <= 120) {
      return diff % 30 == 0;
    }
    return false;
  }

  @RequiredArgsConstructor
  public static class TPAllOutside extends BukkitRunnable {
    private final GameManager manager;
    private final Set<UUID> handlingPlayers = Collections.synchronizedSet(new LinkedHashSet<>());

    public void tpAllOutside() {
      for (Player player : manager.getActiveWorld().getPlayers()) {
        teleport(player, null);
      }
    }

    public boolean isInside(Player player) {
      double minX =
          player.getWorld().getWorldBorder().getCenter().getX()
              - (player.getWorld().getWorldBorder().getSize() / 2);
      double maxX =
          player.getWorld().getWorldBorder().getCenter().getX()
              + (player.getWorld().getWorldBorder().getSize() / 2);
      double minZ =
          player.getWorld().getWorldBorder().getCenter().getZ()
              - (player.getWorld().getWorldBorder().getSize() / 2);
      double maxZ =
          player.getWorld().getWorldBorder().getCenter().getZ()
              + (player.getWorld().getWorldBorder().getSize() / 2);
      return !(player.getLocation().getX() <= minX
          || player.getLocation().getX() >= maxX
          || player.getLocation().getZ() <= minZ
          || player.getLocation().getZ() >= maxZ);
    }

    public boolean isInside(Player player, double borderSize) {
      double minX = player.getWorld().getWorldBorder().getCenter().getX() - (borderSize / 2);
      double maxX = player.getWorld().getWorldBorder().getCenter().getX() + (borderSize / 2);
      double minZ = player.getWorld().getWorldBorder().getCenter().getZ() - (borderSize / 2);
      double maxZ = player.getWorld().getWorldBorder().getCenter().getZ() + (borderSize / 2);
      return !(player.getLocation().getX() <= minX
          || player.getLocation().getX() >= maxX
          || player.getLocation().getZ() <= minZ
          || player.getLocation().getZ() >= maxZ);
    }

    public Location getCorrectedPosition(Location player) {
      double minX =
          player.getWorld().getWorldBorder().getCenter().getX()
              - (player.getWorld().getWorldBorder().getSize() / 2);
      double maxX =
          player.getWorld().getWorldBorder().getCenter().getX()
              + (player.getWorld().getWorldBorder().getSize() / 2);
      double minZ =
          player.getWorld().getWorldBorder().getCenter().getZ()
              - (player.getWorld().getWorldBorder().getSize() / 2);
      double maxZ =
          player.getWorld().getWorldBorder().getCenter().getZ()
              + (player.getWorld().getWorldBorder().getSize() / 2);
      double x = player.getX();
      double y = player.getY();
      double z = player.getZ();
      if (x <= minX) {
        x = minX + 1;
      } else if (x >= maxX) {
        x = maxX - 1;
      }
      if (z <= minZ) {
        z = minZ + 1;
      } else if (z >= maxZ) {
        z = maxZ - 1;
      }
      int ixLoc = Location.locToBlock(x);
      int izLoc = Location.locToBlock(z);
      Chunk tChunk = player.getWorld().getChunkAt(blockToChunk(ixLoc), blockToChunk(izLoc));
      if (!tChunk.isLoaded()) tChunk.load();
      y = MiscUtils.getSafeY(player.getWorld(), ixLoc, Location.locToBlock(y), izLoc);
      if (y == -1) return null;
      return new Location(
          player.getWorld(),
          Math.floor(x) + 0.5,
          y,
          Math.floor(z) + 0.5,
          player.getYaw(),
          player.getPitch());
    }

    public int blockToChunk(int blockVal) { // 1 chunk is 16x16 blocks
      return blockVal >> 4; // ">>4" == "/16"
    }

    public Location teleport(Player player, Location targetLoc) {
      if (player == null || !player.isOnline()) return null;
      Location loc = (targetLoc == null) ? player.getLocation().clone() : targetLoc;
      World world = loc.getWorld();
      if (world == null) {
        return null;
      }
      if (isInside(player)) {
        return null;
      }
      if (handlingPlayers.contains(player.getUniqueId())) {
        return null;
      }
      handlingPlayers.add(player.getUniqueId());

      Location newLoc = getLocation(player, loc);
      if (newLoc == null) {
        return null;
      }

      player.teleport(newLoc);
      player.sendMessage("You have been teleported §binside of the border§f.");

      handlingPlayers.remove(player.getUniqueId());
      return newLoc;
    }

    public Location getLocation(Player player, Location targetLoc) {
      Location newLoc = getCorrectedPosition(targetLoc);
      if (newLoc == null) {
        newLoc = player.getWorld().getSpawnLocation();
      }
      return newLoc;
    }

    @Override
    public void run() {
      tpAllOutside();
    }
  }
}
