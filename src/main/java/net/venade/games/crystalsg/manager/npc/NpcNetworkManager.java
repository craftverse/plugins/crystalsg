package net.venade.games.crystalsg.manager.npc;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.minecraft.server.v1_16_R3.EnumProtocol;
import net.minecraft.server.v1_16_R3.EnumProtocolDirection;
import net.minecraft.server.v1_16_R3.NetworkManager;
import net.minecraft.server.v1_16_R3.Packet;
import net.minecraft.server.v1_16_R3.PacketListener;

import javax.annotation.Nullable;
import javax.crypto.Cipher;
import java.net.SocketAddress;

public class NpcNetworkManager extends NetworkManager {
  public NpcNetworkManager() {
    super(EnumProtocolDirection.SERVERBOUND);
  }

  @Override
  public void channelActive(ChannelHandlerContext channelhandlercontext) throws Exception {}

  @Override
  public void setProtocol(EnumProtocol enumprotocol) {}

  @Override
  public void channelInactive(ChannelHandlerContext channelhandlercontext) throws Exception {}

  @Override
  public void exceptionCaught(ChannelHandlerContext channelhandlercontext, Throwable throwable) {}

  @Override
  protected void channelRead0(ChannelHandlerContext channelhandlercontext, Packet<?> packet)
      throws Exception {}

  @Override
  public void setPacketListener(PacketListener packetlistener) {}

  @Override
  public void sendPacket(Packet<?> packet) {}

  @Override
  public void sendPacket(
      Packet<?> packet,
      @Nullable GenericFutureListener<? extends Future<? super Void>> genericfuturelistener) {}

  @Override
  public void a() {}

  @Override
  public SocketAddress getSocketAddress() {
    return new SocketAddress() {};
  }

  @Override
  public boolean isLocal() {
    return false;
  }

  @Override
  public void a(Cipher cipher, Cipher cipher1) {}

  @Override
  public boolean isConnected() {
    return true;
  }

  @Override
  public void stopReading() {}

  @Override
  public void setCompressionLevel(int i) {}

  @Override
  public void handleDisconnection() {}

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {}

  @Override
  public void channelRegistered(ChannelHandlerContext ctx) throws Exception {}

  @Override
  public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {}

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {}

  @Override
  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {}

  @Override
  public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {}

  @Override
  public void handlerAdded(ChannelHandlerContext ctx) throws Exception {}

  @Override
  public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {}
}
