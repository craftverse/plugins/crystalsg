package net.venade.games.crystalsg.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import net.venade.maps.MapAPI;

/**
 * @author JakeMT04
 * @since 04/08/2021
 */
@AllArgsConstructor
public class ScoreboardManager {
  private final GameManager manager;

  public void updateScoreboard(CrystalSGPlayer player) {
    long now = System.currentTimeMillis();
    ICorePlayer corePlayer = player.getCorePlayer();
    if (corePlayer == null) {
      return;
    }
    VenadeBoard builder = player.getScoreBuilder();
    builder.updateTitle(corePlayer.getLanguageString("crystal.scoreboard.title"));
    List<String> lines = new ArrayList<>();
    lines.add("§r                      §r");
    lines.add(corePlayer.getLanguageString("crystal.scoreboard.game"));
    GameManager.GameState state = manager.getState();
    if (!state.isStarted() || state == GameManager.GameState.STARTING) {
      if (state == GameManager.GameState.WAITING_NOT_ENOUGH) {
        long required =
            this.manager.getPlayersRequiredToStart() - this.manager.getTotalPlayerCount();
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.wait.required." + (required == 1 ? "single" : "plural"),
                required));
        lines.add(corePlayer.getLanguageString("crystal.scoreboard.wait.required"));
      } else if (state == GameManager.GameState.STARTING) {
        long time = this.manager.getTimeToStart() - now;
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.wait.starting", MiscUtils.millisToTimer(time)));
      } else {
        long time = this.manager.getTimeToStart() - now;
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.wait.start", MiscUtils.millisToTimer(time)));
        lines.add(corePlayer.getLanguageString("crystal.scoreboard.wait.start2"));
      }
      lines.add("");
      if (state == GameManager.GameState.STARTING) {
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.wait.players2", this.manager.getTotalPlayerCount()));
      } else {
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.wait.players",
                this.manager.getTotalPlayerCount(),
                this.manager.getMaxPlayers()));
      }
      String mapName =
          MapAPI.getPlayMap() == null
              ? corePlayer.getLanguageString("crystal.scoreboard.wait.map.voting")
              : MapAPI.getPlayMap().getName();
      lines.add(corePlayer.getLanguageString("crystal.scoreboard.wait.map", mapName));

    } else if (state == GameManager.GameState.FINISHED) {
      String elapsed =
          MiscUtils.millisToTimer(this.manager.getFinishedAt() - this.manager.getStartedAt());
      lines.add(corePlayer.getLanguageString("crystal.scoreboard.elapsed", elapsed));
      if (!this.manager.isDeathmatch()) {
        long size = (long) (this.manager.getActiveWorld().getWorldBorder().getSize() / 2);
        lines.add(corePlayer.getLanguageString("crystal.scoreboard.border", size));
      }
      if (this.manager.getWinner() != null) {
        lines.add(
            corePlayer.getLanguageString(
                "crystal.scoreboard.finish.winner", this.manager.getWinner().getBase().getName()));
      }
      lines.add(corePlayer.getLanguageString("crystal.scoreboard.kills", player.getKills()));
    } else {
      String elapsed = MiscUtils.millisToTimer(now - this.manager.getStartedAt());
      lines.add(corePlayer.getLanguageString("crystal.scoreboard.elapsed", elapsed));
      if (!this.manager.isDeathmatch()) {
        long size = (long) (this.manager.getActiveWorld().getWorldBorder().getSize() / 2);
        String line = corePlayer.getLanguageString("crystal.scoreboard.border", size);
        long nextShrink = this.manager.getBorderShrinkTask().getTimeToNextShrink(now);
        if (nextShrink != 0) {
          line += " §7[§b" + nextShrink + "§7]";
        }
        lines.add(line);
      }
      lines.add(
          corePlayer.getLanguageString(
              "crystal.scoreboard.alive", this.manager.getAlivePlayerCount()));
      lines.add(corePlayer.getLanguageString("crystal.scoreboard.kills", player.getKills()));
      Map.Entry<String, Long> event = manager.getNextEvent(now);
      if (event != null) {
        lines.add("");
        lines.add(corePlayer.getLanguageString("crystal.scoreboard.next"));
        lines.add(
            corePlayer.getLanguageString(
                event.getKey(), MiscUtils.millisToTimer(event.getValue())));
      }
    }
    lines.add("");
    lines.add("§7§owww.venade.net");
    lines.replaceAll(HexColor::translate);
    builder.updateLines(lines);
  }
}
