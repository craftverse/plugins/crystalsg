package net.venade.games.crystalsg.manager;

import lombok.AllArgsConstructor;
import net.venade.games.crystalsg.player.CrystalSGPlayer;

/**
 * @author JakeMT04
 * @since 18/08/2021
 */
@AllArgsConstructor
public class VisibilityManager {
  private final GameManager manager;

  public void processPlayer(CrystalSGPlayer player) {
    for (CrystalSGPlayer otherPlayer : manager.getPlayers().values()) {
      if (otherPlayer.getState().isSpectating()) {
        player.getBase().hidePlayer(manager.getPlugin(), otherPlayer.getBase());
      } else {
        player.getBase().showPlayer(manager.getPlugin(), otherPlayer.getBase());
      }
      if (player.getState().isSpectating()) {
        otherPlayer.getBase().hidePlayer(manager.getPlugin(), player.getBase());
      } else {
        otherPlayer.getBase().showPlayer(manager.getPlugin(), player.getBase());
      }
    }
  }
}
