package net.venade.games.crystalsg.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.block.data.Rotatable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 01/08/2021
 */
@Getter
@Setter
public class CrystalSGPlayer {
  private Player base;
  private ICorePlayer corePlayer;
  private VenadeBoard scoreBuilder;
  private Location frozen = null;
  private PlayerState state = PlayerState.PLAYING;
  private ItemStack[] oldInventoryContents;
  private Location deathLocation;
  private String votedMap = "";
  private long combatLogDeathTime = 0;
  private int kills = 0;

  public CrystalSGPlayer(Player base) {
    this.base = base;
    this.scoreBuilder = new VenadeBoard(base);
  }

  public static BlockFace getCardinalDirection(Player player) {
    double rotation = (player.getLocation().getYaw() - 90.0F) % 360.0F;
    if (rotation < 0.0D) {
      rotation += 360.0D;
    }
    if ((0.0D <= rotation) && (rotation < 22.5D)) {
      return BlockFace.EAST;
    }
    if ((22.5D <= rotation) && (rotation < 67.5D)) {
      return BlockFace.SOUTH_EAST;
    }
    if ((67.5D <= rotation) && (rotation < 112.5D)) {
      return BlockFace.SOUTH;
    }
    if ((112.5D <= rotation) && (rotation < 157.5D)) {
      return BlockFace.SOUTH_WEST;
    }
    if ((157.5D <= rotation) && (rotation < 202.5D)) {
      return BlockFace.WEST;
    }
    if ((202.5D <= rotation) && (rotation < 247.5D)) {
      return BlockFace.NORTH_WEST;
    }
    if ((247.5D <= rotation) && (rotation < 292.5D)) {
      return BlockFace.NORTH;
    }
    if ((292.5D <= rotation) && (rotation < 337.5D)) {
      return BlockFace.NORTH_EAST;
    }
    return BlockFace.EAST;
  }

  public ICorePlayer getCorePlayer() {
    if (this.corePlayer != null) {
      return this.corePlayer;
    }
    this.corePlayer = VenadeAPI.getPlayer(this.base.getUniqueId());
    return this.corePlayer;
  }

  public boolean isOffline() {
    return state.name().startsWith("LOGGED");
  }

  public void saveInventory() {
    this.deathLocation = this.base.getLocation().clone();
    this.oldInventoryContents = this.base.getInventory().getContents();
  }

  public void saveInventory(Player otherEntity) {
    this.deathLocation = otherEntity.getLocation().clone();
    this.oldInventoryContents = otherEntity.getInventory().getContents();
  }

  public void createHead(Location loc) {
    loc.getBlock().setType(Material.OAK_FENCE);
    Block block = loc.clone().add(0, 1, 0).getBlock();
    block.setType(Material.PLAYER_HEAD);
    Skull skull = (Skull) block.getState();
    skull.setOwningPlayer(this.base);
    skull.update(true);
    Rotatable rotatable = (Rotatable) block.getBlockData();
    rotatable.setRotation(getCardinalDirection(this.base));
    block.setBlockData(rotatable, true);
  }

  @AllArgsConstructor
  @Getter
  public enum PlayerState {
    PLAYING,
    DEAD,
    SPEC,
    LOGGED_ALIVE,
    LOGGED_DEAD,
    LOGGED_DIED,
    ;

    public boolean isSpectating() {
      return this != PLAYING;
    }
  }
}
