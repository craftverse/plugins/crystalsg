package net.venade.games.crystalsg.cmd.loot;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.cmd.loot.chest.ChestSubCommand;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class LootSubCommand extends VenadeCommand {
  public LootSubCommand(CrystalSGPlugin plugin) {
    super("loot", "crystal.command.loot", null, true);
    registerSubCommand(new AddLootableItem(plugin));
    registerSubCommand(new ChestSubCommand(plugin));
  }
}
