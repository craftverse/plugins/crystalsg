package net.venade.games.crystalsg.listener;

import lombok.AllArgsConstructor;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@AllArgsConstructor
public class PlayerMovementListener implements Listener {
  private final GameManager manager;

  @EventHandler
  public void onMove(PlayerMoveEvent event) {
    CrystalSGPlayer player = manager.getPlayers().get(event.getPlayer().getUniqueId());
    if (player == null) {
      event.getPlayer().kickPlayer("Something went very wrong.");
      return;
    }
    if (player.getFrozen() != null) {
      Location lockedTo = player.getFrozen();
      Location current = player.getBase().getLocation();
      if (current.getBlockX() != lockedTo.getBlockX()
          || current.getBlockZ() != lockedTo.getBlockZ()) {
        event.setTo(lockedTo);
      }
    }
  }
}
