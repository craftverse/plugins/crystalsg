package net.venade.games.crystalsg.manager;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.service.service.ServerState;
import com.google.common.collect.Maps;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.games.crystalsg.inventory.MapVotingInventory;
import net.venade.games.crystalsg.listener.PlayerDeathListener;
import net.venade.games.crystalsg.loot.LootGenerator;
import net.venade.games.crystalsg.manager.npc.NpcListener;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.games.crystalsg.task.BorderShrinkTask;
import net.venade.games.crystalsg.task.CountdownTask;
import net.venade.games.crystalsg.task.StartTask;
import net.venade.internal.api.inventory.item.ItemBuilder;
import net.venade.maps.MapAPI;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@Getter
@Setter
public class GameManager {
  private final CrystalSGPlugin plugin;
  private final HashMap<UUID, CrystalSGPlayer> players = new HashMap<>();
  private final LootGenerator lootGenerator;
  private final ScoreboardManager scoreboardManager;
  private final VisibilityManager visibilityManager;
  private final BorderShrinkTask borderShrinkTask;
  private final MapVotingInventory mapVotingInventory;
  private GameState state = GameState.WAITING_NOT_ENOUGH;
  private long playersAtStart = 0, timeToStart = 0, startedAt = 0, finishedAt = 0;
  private StartTask startTask;
  private boolean isPvPEnabled = false, deathmatch = false;
  private CountdownTask pvpEnableTask, feastSpawnTask, deathmatchTask;
  private CrystalSGPlayer winner;

  public GameManager(CrystalSGPlugin plugin) {
    this.plugin = plugin;
    this.lootGenerator = new LootGenerator(this);
    this.scoreboardManager = new ScoreboardManager(this);
    this.visibilityManager = new VisibilityManager(this);
    plugin.getServer().getPluginManager().registerEvents(new NpcListener(this.plugin), plugin);
    plugin.getServer().getPluginManager().registerEvents(new PlayerDeathListener(this), plugin);
    this.borderShrinkTask = new BorderShrinkTask(this.plugin, this);
    this.startTask = new StartTask(this);
    this.startTask.runTaskTimer(plugin, 10L, 10L);
    this.mapVotingInventory = new MapVotingInventory(this);
  }

  public void broadcast(String key, Object... args) {
    this.plugin.broadcast(key, args);
  }

  public List<CrystalSGPlayer> getAlivePlayers() {
    return this.players.values().stream()
        .filter(
            s ->
                s.getState() == CrystalSGPlayer.PlayerState.PLAYING
                    || s.getState() == CrystalSGPlayer.PlayerState.LOGGED_ALIVE)
        .collect(Collectors.toList());
  }

  public Map.Entry<String, Long> getNextEvent(long now) {
    if (this.state != GameState.STARTED) {
      return null;
    }
    if (this.getTimeToPvP() > now) {
      long toPvP = this.getTimeToPvP() - now;
      return Maps.immutableEntry("crystal.scoreboard.event.pvp", toPvP);
      // return "PvP Enabled: §b" + MiscUtils.millisToTimer(toPvP);
    } else if (this.getTimeToFeast() > now) {
      long toFeast = this.getTimeToFeast() - now;
      return Maps.immutableEntry("crystal.scoreboard.event.feast", toFeast);
      // return "Feast: §b" + MiscUtils.millisToTimer(toFeast);
    } else if (this.getTimeToDeathmatch() > now) {
      long toDm = this.getTimeToDeathmatch() - now;
      // return "Deathmatch: §b" + MiscUtils.millisToTimer(toDm);
      return Maps.immutableEntry("crystal.scoreboard.event.deathmatch", toDm);
    }
    return null;
  }

  public void setupWorld() {
    try {
      this.lootGenerator.removeFeast();
    } catch (Exception ignored) {
    }
    try {
      Location location = MapAPI.getPlayMap().getLocation("spawn");
      assert location != null;
      this.getActiveWorld().getWorldBorder().setCenter(location);
      this.getActiveWorld().getWorldBorder().setSize(1001);
      this.getActiveWorld().getWorldBorder().setDamageAmount(0);
      this.getActiveWorld().getWorldBorder().setDamageBuffer(Double.MAX_VALUE);
      this.getActiveWorld().getWorldBorder().setWarningDistance(0);
      this.getActiveWorld().getWorldBorder().setWarningTime(0);

      this.getActiveWorld().setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
      this.getActiveWorld().setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
      this.getActiveWorld().setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
      this.getActiveWorld().setGameRule(GameRule.DO_WEATHER_CYCLE, false);
      this.getActiveWorld().setGameRule(GameRule.SPAWN_RADIUS, 0);
      this.getActiveWorld().setGameRule(GameRule.DO_MOB_SPAWNING, false);
      for (Entity entity : this.getActiveWorld().getEntities()) {
        entity.remove();
      }
    } catch (Exception ignored) {
    }
  }

  public void teleportIntoWorld() {
    this.setupWorld();
    this.lootGenerator.populateChests();
    state = GameState.STARTING;
    Location location = MapAPI.getPlayMap().getLocation("spawn");
    for (CrystalSGPlayer csgPlayer : this.players.values()) {
      if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.PLAYING) {
        csgPlayer.getBase().teleport(location);
        csgPlayer
            .getBase()
            .addPotionEffect(
                new PotionEffect(
                    PotionEffectType.INVISIBILITY,
                    Integer.MAX_VALUE,
                    Integer.MAX_VALUE,
                    true,
                    false,
                    false));
        csgPlayer.setFrozen(location);
        csgPlayer.getBase().getInventory().clear();
      }
    }

    this.playersAtStart = this.getTotalPlayerCount();
    this.timeToStart = System.currentTimeMillis() + 11_000L;
    this.startTask.setBroadcastNext(10);
    CubidCloud.getBridge().getLocalService().setServerState(ServerState.INGAME);
    CubidCloud.getBridge().updateLocalService();
  }

  public void startGame() {
    this.startTask.cancel();
    this.state = GameState.STARTED;
    this.startedAt = (System.currentTimeMillis() / 1000) * 1000;
    for (CrystalSGPlayer csgPlayer : this.players.values()) {
      if (csgPlayer.getState() == CrystalSGPlayer.PlayerState.PLAYING) {
        csgPlayer.getBase().removePotionEffect(PotionEffectType.INVISIBILITY);
        csgPlayer
            .getBase()
            .addPotionEffect(
                new PotionEffect(
                    PotionEffectType.INVISIBILITY, 30 * 20, Integer.MAX_VALUE, true, false, false));
        csgPlayer.setFrozen(null);
        csgPlayer.getBase().setGameMode(GameMode.SURVIVAL);
      }
    }
    this.pvpEnableTask =
        new CountdownTask(
            this,
            this.getTimeToPvP(),
            true,
            "crystal.pvp.in",
            () -> {
              this.setPvPEnabled(true);
              this.broadcast("crystal.pvp.enabled");
              this.feastSpawnTask.runTaskTimerAsynchronously(this.plugin, 10L, 10L);
            });
    this.feastSpawnTask =
        new CountdownTask(
            this,
            this.getTimeToFeast(),
            true,
            "crystal.feast.in",
            () ->
                this.plugin
                    .getServer()
                    .getScheduler()
                    .runTask(
                        this.plugin,
                        () -> {
                          this.deathmatchTask.runTaskTimerAsynchronously(this.plugin, 10L, 10L);
                          this.lootGenerator.populateFeast();
                          this.broadcast("crystal.feast.spawned");
                        }));
    this.deathmatchTask =
        new CountdownTask(
            this,
            this.getTimeToDeathmatch(),
            true,
            "crystal.deathmatch.in",
            () -> {
              this.plugin
                  .getServer()
                  .getScheduler()
                  .runTask(
                      this.plugin,
                      () -> {
                        this.lootGenerator.removeFeast();
                        this.getActiveWorld().getWorldBorder().setSize(41);
                      });
              this.deathmatch = true;
              this.broadcast("crystal.deathmatch.started");
            });
    this.pvpEnableTask.runTaskTimerAsynchronously(this.plugin, 10L, 10L);
    this.borderShrinkTask.runTaskTimerAsynchronously(this.plugin, 10L, 10L);
    new BorderShrinkTask.TPAllOutside(this).runTaskTimer(this.plugin, 5L, 5L);
  }

  public long getTimeToPvP() {
    return this.startedAt + 151_000L;
  }

  public long getTimeToFeast() {
    return this.startedAt + 601_000L;
  }

  public long getTimeToDeathmatch() {
    return this.startedAt + 1_201_000;
  }

  public long getTotalPlayerCount() {
    return players.values().stream()
        .filter(p -> p.getState() == CrystalSGPlayer.PlayerState.PLAYING)
        .count();
  }

  public World getActiveWorld() {
    if (MapAPI.getPlayMap() == null) {
      return null;
    }
    return this.plugin.getServer().getWorld(MapAPI.getPlayMap().getName());
  }

  public long getPlayersRequiredToStart() {
    return this.getMaxPlayers() / 4;
  }

  public int getTotalOnline() {
    return plugin.getServer().getOnlinePlayers().size();
  }

  public long getMaxPlayers() {
    if (this.state.isStarted()) {
      return this.playersAtStart;
    } else {
      return this.plugin.getServer().getMaxPlayers();
    }
  }

  public void applySpectatorSettings(CrystalSGPlayer player, String reason) {
    player
        .getCorePlayer()
        .sendMessage("crystal.spectator", player.getCorePlayer().getLanguageString(reason));
    this.visibilityManager.processPlayer(player);
    player.getBase().setGameMode(GameMode.ADVENTURE);
    player.getBase().setAllowFlight(true);
    player.getBase().setFlying(true);
    player.getBase().getInventory().clear();
    player.getBase().getInventory().setArmorContents(null);
    ItemStack compass =
        ItemBuilder.builder(Material.COMPASS)
            .display(player.getCorePlayer().getLanguageString("crystal.item.compass"))
            .build();
    player.getBase().getInventory().setItem(4, compass);
    player.getBase().getInventory().setHeldItemSlot(4);
    this.plugin
        .getServer()
        .getScheduler()
        .runTaskLater(
            this.plugin,
            () ->
                player
                    .getBase()
                    .addPotionEffect(
                        new PotionEffect(
                            PotionEffectType.INVISIBILITY,
                            Integer.MAX_VALUE,
                            Integer.MAX_VALUE,
                            true,
                            false,
                            false)),
            1L);
  }

  public void checkForWin() {
    List<CrystalSGPlayer> alivePlayers = this.getAlivePlayers();
    if (alivePlayers.size() <= 1) {
      // end the game
      this.state = GameState.FINISHED;
      this.finishedAt = System.currentTimeMillis();
      try {
        this.pvpEnableTask.cancel();
        this.feastSpawnTask.cancel();
        this.borderShrinkTask.cancel();
      } catch (IllegalStateException ignored) {
      }
      new CountdownTask(
              this,
              this.finishedAt + 61_000L,
              false,
              "crystal.reboot.in",
              () -> {
                // TODO send all players to hub
                this.plugin.getServer().shutdown();
              })
          .runTaskTimer(this.plugin, 10L, 10L);
      alivePlayers.stream()
          .findFirst()
          .ifPresent(
              player -> {
                this.broadcast(
                    "crystal.winner",
                    player.getBase().getName()); // TODO more elaborate end message
                player.getBase().sendTitle("§aYou win!", "Congratulations!", 10, 70, 20);
                this.winner = player;
              });
    }
  }

  public void registerPlayer(CrystalSGPlayer csgPlayer) {
    Player player = csgPlayer.getBase();
    this.players.put(csgPlayer.getBase().getUniqueId(), csgPlayer);
    if (!this.state.isStarted()) {
      if (this.getTotalPlayerCount() >= this.getPlayersRequiredToStart()) {
        this.state = GameState.WAITING_ENOUGH;
        if (this.getTotalPlayerCount() == this.getMaxPlayers()) {
          long diff = this.timeToStart - System.currentTimeMillis();
          if (diff >= 60_000L) {
            this.timeToStart = System.currentTimeMillis() + 61_000L;
            this.startTask.setBroadcastNext(60);
          }
        } else {
          this.timeToStart = System.currentTimeMillis() + 121_000L;
        }
      }
      player.getInventory().clear();
      player.getInventory().setArmorContents(null);
      player
          .getAttribute(Attribute.GENERIC_MAX_HEALTH)
          .setBaseValue(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
      player.setHealth(20);
      player.setFoodLevel(20);
      player.setGameMode(GameMode.ADVENTURE);
      player.getActivePotionEffects().forEach(p -> player.removePotionEffect(p.getType()));
      player
          .getInventory()
          .setItem(
              0,
              ItemBuilder.builder(Material.PAPER)
                  .display(csgPlayer.getCorePlayer().getLanguageString("crystal.hotbar.voting"))
                  .build());
      player
          .getInventory()
          .setItem(
              8,
              ItemBuilder.builder(Material.DRAGON_BREATH)
                  .display(csgPlayer.getCorePlayer().getLanguageString("crystal.hotbar.hub"))
                  .build());
    }
    player.teleport(MapAPI.getWaitingHub().getLocation("spawn"));
  }

  public void unregisterPlayer(Player player) {
    CrystalSGPlayer csgPlayer = this.players.remove(player.getUniqueId());
    this.mapVotingInventory.removePlayer(csgPlayer);
    if (!this.getState().isStarted()) {
      if (this.getTotalPlayerCount() < this.getPlayersRequiredToStart()) {
        this.state = GameState.WAITING_NOT_ENOUGH;
        this.timeToStart = 0;
        this.startTask.setBroadcastNext(60);
      }
    }
  }

  public int getAlivePlayerCount() {
    return this.getAlivePlayers().size();
  }

  @AllArgsConstructor
  @Getter
  public enum GameState {
    WAITING_NOT_ENOUGH(false),
    WAITING_ENOUGH(false),
    STARTING(true),
    STARTED(true),
    FINISHED(true);

    private final boolean started;
  }
}
