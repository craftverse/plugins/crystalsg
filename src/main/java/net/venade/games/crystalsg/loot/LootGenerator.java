package net.venade.games.crystalsg.loot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import lombok.RequiredArgsConstructor;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.maps.MapAPI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Container;
import org.bukkit.block.data.Directional;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@RequiredArgsConstructor
public class LootGenerator {
  private static final int[][] LOCATIONS =
      new int[][] {
        {4, 4},
        {-4, 4},
        {-4, -4},
        {4, -4},
        {8, 0},
        {0, 8},
        {-8, 0},
        {0, -8},
        {9, 9},
        {-9, 9},
        {-9, -9},
        {9, -9},
      };

  private final GameManager manager;

  public void populateFeast() {
    Location loc = MapAPI.getPlayMap().getLocation("feast");
    if (loc == null) {
      throw new IllegalStateException("No feast location is defined for this map");
    }
    // teleport players inside outside
    for (CrystalSGPlayer csgPlayer : this.manager.getAlivePlayers()) {
      if (csgPlayer.isOffline()) continue;
      Player player = csgPlayer.getBase();
      if (MiscUtils.isInside(player, loc, 12)) {
        Location destination = MiscUtils.getCorrectedPosition(player.getLocation(), loc, 12);
        if (destination == null) {
          continue;
        }
        player.teleport(destination);
        player.sendMessage("§cYou have been teleported away from the feast.");
      }
    }
    loc.getBlock().setType(Material.ENCHANTING_TABLE);
    loc.clone().add(2, 0, 0).getBlock().setType(Material.BOOKSHELF);
    loc.clone().add(-2, 0, 0).getBlock().setType(Material.BOOKSHELF);
    loc.clone().add(0, 0, 2).getBlock().setType(Material.BOOKSHELF);
    loc.clone().add(0, 0, -2).getBlock().setType(Material.BOOKSHELF);
    Iterator<int[]> iterator = Arrays.stream(LOCATIONS).iterator();
    new BukkitRunnable() {
      public void run() {
        if (iterator.hasNext()) {
          int[] amount = iterator.next();
          int x = amount[0];
          int z = amount[1];
          Location newLoc = loc.clone().add(x, 0, z);
          try {
            Block block = newLoc.getBlock();
            block.setType(Material.BARREL);
            Directional data = (Directional) block.getBlockData();
            data.setFacing(BlockFace.UP);
            block.setBlockData(data);
            Container state = (Container) block.getState();
            state.getInventory().clear();
            populateChest(state, true);
            newLoc.getWorld().playSound(newLoc, Sound.BLOCK_END_PORTAL_FRAME_FILL, 1, 1);
          } catch (Exception e) {
            e.printStackTrace();
            manager.getPlugin().getLogger().warning("Failed to populate chest");
          }
        } else {
          cancel();
        }
      }
    }.runTaskTimer(manager.getPlugin(), 1, 1);
  }

  public void populateChests() {
    // TODO redo the way that this fetches chest locations to use the MapAPI, somehow
    List<String> locations =
        this.manager
            .getPlugin()
            .getLootConfig()
            .getStringList("chests." + manager.getActiveWorld().getName());
    for (String location : locations) {
      String[] parts = location.split(";");
      try {
        int x = Integer.parseInt(parts[0]);
        int y = Integer.parseInt(parts[1]);
        int z = Integer.parseInt(parts[2]);
        Block block = this.manager.getActiveWorld().getBlockAt(x, y, z);
        block.setType(Material.BARREL);
        Container state = (Container) block.getState();
        state.getInventory().clear();
        this.populateChest(state, false);

      } catch (Exception e) {
        e.printStackTrace();
        manager.getPlugin().getLogger().warning("Failed to populate chest");
      }
    }
  }

  public void populateChest(Container chest, boolean feast) {
    List<ItemStack> items = this.generateItems(feast);
    int slotsInChest = chest.getInventory().getSize();
    for (ItemStack item : items) {
      if (chest.getInventory().firstEmpty() == -1) {
        throw new IllegalArgumentException("Chest does not have enough space");
      }
      int slot = new Random().nextInt(slotsInChest);
      while (chest.getInventory().getItem(slot) != null) {
        slot = new Random().nextInt(slotsInChest);
      }
      chest.getInventory().setItem(slot, item);
    }
  }

  public List<LootItem> getLootItems(boolean feast) {
    List<LootItem> items = new ArrayList<>();
    ConfigurationSection section =
        manager.getPlugin().getLootConfig().getConfigurationSection(feast ? "feast" : "table");
    if (section == null) {
      return items;
    }
    for (String key : section.getKeys(false)) {
      ConfigurationSection itemConfig = section.getConfigurationSection(key);
      assert itemConfig != null;
      ItemStack item = itemConfig.getItemStack("item");
      int min = itemConfig.getInt("min");
      int max = itemConfig.getInt("max");
      double percent = itemConfig.getDouble("percent");
      items.add(new LootItem(item, percent, min, max));
    }
    return items;
  }

  public List<ItemStack> generateItems(boolean feast) {
    int minItems =
        this.manager.getPlugin().getLootConfig().getInt((feast ? "feast" : "chest") + ".min", 2);
    int maxItems =
        this.manager.getPlugin().getLootConfig().getInt((feast ? "feast" : "chest") + ".max", 5);
    Random random = new Random();
    int number = random.nextInt(maxItems - minItems) + minItems;
    List<ItemStack> items = new ArrayList<>();
    for (int i = 0; i < number; i++) {
      LootItem item = this.generateOne(feast);
      int amount;
      if (item.getMax() == item.getMin()) {
        amount = item.getMax();
      } else {
        amount = random.nextInt(item.getMax() - item.getMin()) + item.getMin();
      }
      ItemStack stack = item.getItem().clone();
      stack.setAmount(amount);
      items.add(stack);
    }
    return items;
  }

  public LootItem generateOne(boolean feast) {
    if (this.getLootItems(feast).isEmpty()) {
      throw new IllegalStateException("There are no items to fill the container with");
    }
    while (true) {
      List<LootItem> allItems = this.getLootItems(feast);
      Collections.shuffle(allItems);
      double totalWeight = 0;
      for (LootItem item : allItems) {
        totalWeight += item.getPercentage();
      }
      double random = Math.random() * totalWeight;
      for (LootItem allItem : allItems) {
        random -= allItem.getPercentage();
        if (random <= 0.0d) {
          return allItem;
        }
      }
    }
  }

  public void removeFeast() {
    Location loc = MapAPI.getPlayMap().getLocation("feast");
    if (loc == null) {
      throw new IllegalStateException("No feast location is defined for this map");
    }
    this.checkAndRemove(loc.getBlock(), Material.ENCHANTING_TABLE);
    this.checkAndRemove(loc.clone().add(2, 0, 0).getBlock(), Material.BOOKSHELF);
    this.checkAndRemove(loc.clone().add(-2, 0, 0).getBlock(), Material.BOOKSHELF);
    this.checkAndRemove(loc.clone().add(0, 0, 2).getBlock(), Material.BOOKSHELF);
    this.checkAndRemove(loc.clone().add(0, 0, -2).getBlock(), Material.BOOKSHELF);
    for (int[] diff : LOCATIONS) {
      int x = diff[0];
      int z = diff[1];
      Location newLoc = loc.clone().add(x, 0, z);
      Block block = newLoc.getBlock();
      this.checkAndRemove(block, Material.BARREL);
    }
  }

  private void checkAndRemove(Block block, Material type) {
    if (block.getType() == type) {
      if (block.getState() instanceof Container) {
        ((Container) block.getState()).getInventory().clear();
      }
      block.setType(Material.AIR, false);
    }
  }
}
