package net.venade.games.crystalsg.cmd.loot;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Default;
import net.venade.internal.api.command.annotation.Desc;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class AddLootableItem extends VenadeCommand {
  private final CrystalSGPlugin plugin;

  public AddLootableItem(CrystalSGPlugin plugin) {
    super("additem", "crystal.command.loot.additem", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(
      Sender sender,
      @Desc("crystal.command.loot.additem.min") int min,
      @Desc("crystal.command.loot.additem.max") int max,
      @Desc("crystal.command.loot.additem.percent") double percent,
      @Default("loot") @Desc("crystal.command.loot.additem.location") ItemLocation location) {
    if (sender.isConsole()) {
      return;
    }
    if (percent < 0 || percent > 100) {
      sender.sendRawMessage("§cThe percentage chance must be between 0 and 100");
      return;
    }
    Player player = sender.getAsPlayer();
    ItemStack item = player.getInventory().getItemInMainHand();
    if (item.getType() == Material.AIR) {
      sender.sendRawMessage("§cYou need to hold an item in your hand.");
      return;
    }
    item = item.clone();
    item.setAmount(1);
    if (location == ItemLocation.BOTH || location == ItemLocation.LOOT) {
      ConfigurationSection config = this.plugin.getLootConfig().getConfigurationSection("table");
      if (config == null) {
        config = this.plugin.getLootConfig().createSection("table");
      }
      int key =
          config.getKeys(false).stream().map(Integer::valueOf).max(Integer::compareTo).orElse(0)
              + 1;
      ConfigurationSection section = config.createSection(String.valueOf(key));
      section.set("min", min);
      section.set("max", max);
      section.set("percent", percent);
      section.set("item", item.clone());
    }
    if (location == ItemLocation.BOTH || location == ItemLocation.FEAST) {
      ConfigurationSection config = this.plugin.getLootConfig().getConfigurationSection("feast");
      if (config == null) {
        config = this.plugin.getLootConfig().createSection("feast");
      }
      int key =
          config.getKeys(false).stream().map(Integer::valueOf).max(Integer::compareTo).orElse(0)
              + 1;
      ConfigurationSection section = config.createSection(String.valueOf(key));
      section.set("min", min);
      section.set("max", max);
      section.set("percent", percent);
      section.set("item", item.clone());
    }
    this.plugin.saveLootConfig();
    sender.sendRawMessage(
        "Added §b"
            + item.getType().name()
            + "§f to §b"
            + location.name()
            + "§f with a minimum count of §b"
            + min
            + "§f, maximum of §b"
            + max
            + "§f, and percentage chance of §b"
            + percent
            + "%§f.");
  }
}
