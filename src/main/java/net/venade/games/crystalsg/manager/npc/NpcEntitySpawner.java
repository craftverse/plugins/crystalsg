package net.venade.games.crystalsg.manager.npc;

import com.mojang.datafixers.util.Pair;
import net.minecraft.server.v1_16_R3.EntityPlayer;
import net.minecraft.server.v1_16_R3.EnumItemSlot;
import net.minecraft.server.v1_16_R3.ItemStack;
import net.minecraft.server.v1_16_R3.MinecraftServer;
import net.minecraft.server.v1_16_R3.Packet;
import net.minecraft.server.v1_16_R3.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_16_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_16_R3.WorldServer;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class NpcEntitySpawner {
  public static Player spawn(Player player) {
    NpcPlayer npcPlayer = NpcPlayer.valueOf(player);
    WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
    Location l = player.getLocation();

    npcPlayer.spawnIn(worldServer);
    npcPlayer.setPositionRotation(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
    npcPlayer.playerInteractManager.a(worldServer);
    npcPlayer.invulnerableTicks = 0;

    for (EntityPlayer o : MinecraftServer.getServer().getPlayerList().players) {
      if (o == null || o instanceof NpcPlayer) continue;

      PacketPlayOutPlayerInfo packet =
          new PacketPlayOutPlayerInfo(
              PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npcPlayer);
      o.playerConnection.sendPacket(packet);
    }

    worldServer.addEntity(npcPlayer);
    return npcPlayer.getBukkitEntity();
  }

  public static void despawn(Player player) {
    EntityPlayer entity = ((CraftPlayer) player).getHandle();

    if (!(entity instanceof NpcPlayer)) {
      throw new IllegalArgumentException();
    }

    for (EntityPlayer o : MinecraftServer.getServer().getPlayerList().players) {
      if (o == null || o instanceof NpcPlayer) continue;

      PacketPlayOutPlayerInfo packet =
          new PacketPlayOutPlayerInfo(
              PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entity);
      o.playerConnection.sendPacket(packet);
    }

    WorldServer worldServer = entity.getWorldServer();
    worldServer.removeEntity(entity);
  }

  public static boolean isNpc(Player player) {
    return ((CraftPlayer) player).getHandle() instanceof NpcPlayer;
  }

  public static NpcIdentity getIdentity(Player player) {
    if (!isNpc(player)) {
      throw new IllegalArgumentException();
    }

    return ((NpcPlayer) ((CraftPlayer) player).getHandle()).getNpcIdentity();
  }

  public static void updateEquipment(Player player) {
    EntityPlayer entity = ((CraftPlayer) player).getHandle();

    if (!(entity instanceof NpcPlayer)) {
      throw new IllegalArgumentException();
    }

    Location l = player.getLocation();
    int rangeSquared = 512 * 512;
    List<Pair<EnumItemSlot, ItemStack>> items = new ArrayList<>();

    for (EnumItemSlot slot : EnumItemSlot.values()) {
      ItemStack item = entity.getEquipment(slot);
      if (item == null) continue;
      items.add(new Pair<>(slot, item));
    }

    Packet packet = new PacketPlayOutEntityEquipment(entity.getId(), items);

    for (Object o : entity.world.getPlayers()) {
      if (!(o instanceof EntityPlayer)) continue;

      EntityPlayer p = (EntityPlayer) o;
      Location loc = p.getBukkitEntity().getLocation();
      if (l.getWorld().equals(loc.getWorld()) && l.distanceSquared(loc) <= rangeSquared) {
        p.playerConnection.sendPacket(packet);
      }
    }
  }

  public static void createPlayerList(Player player) {
    EntityPlayer p = ((CraftPlayer) player).getHandle();

    for (WorldServer worldServer : MinecraftServer.getServer().getWorlds()) {
      for (Object o : worldServer.getPlayers()) {
        if (!(o instanceof NpcPlayer)) continue;

        NpcPlayer npcPlayer = (NpcPlayer) o;
        PacketPlayOutPlayerInfo packet =
            new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npcPlayer);
        p.playerConnection.sendPacket(packet);
      }
    }
  }

  public static void removePlayerList(Player player) {
    EntityPlayer p = ((CraftPlayer) player).getHandle();

    for (WorldServer worldServer : MinecraftServer.getServer().getWorlds()) {
      for (Object o : worldServer.getPlayers()) {
        if (!(o instanceof NpcPlayer)) continue;

        NpcPlayer npcPlayer = (NpcPlayer) o;
        PacketPlayOutPlayerInfo packet =
            new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npcPlayer);
        p.playerConnection.sendPacket(packet);
      }
    }
  }
}
