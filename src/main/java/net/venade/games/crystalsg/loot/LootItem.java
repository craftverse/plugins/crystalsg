package net.venade.games.crystalsg.loot;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
@AllArgsConstructor
@Getter
public class LootItem {
  private final ItemStack item;
  private final double percentage;
  private final int min;
  private final int max;
}
