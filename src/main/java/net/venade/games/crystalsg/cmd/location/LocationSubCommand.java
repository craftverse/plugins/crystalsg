package net.venade.games.crystalsg.cmd.location;

import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class LocationSubCommand extends VenadeCommand {
  public LocationSubCommand(CrystalSGPlugin plugin) {
    super("location", "crystal.command.location", null, true);
    registerSubCommand(new SetSpawnLocationCommand(plugin));
    registerSubCommand(new SetLobbyLocationCommand(plugin));
    registerSubCommand(new SetFeastSpawnLocationCommand(plugin));
  }
}
