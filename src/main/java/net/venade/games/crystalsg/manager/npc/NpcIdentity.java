package net.venade.games.crystalsg.manager.npc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class NpcIdentity {
  private final UUID id;
  private final String name;

  public NpcIdentity(Player player) {
    this(player.getUniqueId(), player.getName());
  }
}
