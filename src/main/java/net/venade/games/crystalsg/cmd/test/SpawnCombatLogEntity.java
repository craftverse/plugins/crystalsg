package net.venade.games.crystalsg.cmd.test;

import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.manager.npc.NpcManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Desc;
import net.venade.internal.api.command.annotation.Optional;

/**
 * @author JakeMT04
 * @since 18/08/2021
 */
public class SpawnCombatLogEntity extends VenadeCommand {
  private final GameManager manager;

  public SpawnCombatLogEntity(GameManager manager) {
    super("spawncombatlog", "Spawns a combat log entity for a player", null);
    this.manager = manager;
  }

  @CommandMethod
  public void onCommand(
      Sender sender, @Desc("The player to spawn the entity for") @Optional CrystalSGPlayer player) {
    if (player == null) {
      if (sender.isConsole()) {
        sender.sendRawMessage(buildHelpMessage(sender));
        return;
      }
      player = this.manager.getPlayers().get(sender.getAsPlayer().getUniqueId());
    }
    NpcManager.spawn(player.getBase());
    sender.sendRawMessage(
        "Attempted to spawn a combat log entity for §b" + player.getBase().getName() + "§f.");
  }
}
