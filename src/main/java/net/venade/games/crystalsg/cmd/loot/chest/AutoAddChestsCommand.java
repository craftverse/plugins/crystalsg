package net.venade.games.crystalsg.cmd.loot.chest;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import net.venade.games.crystalsg.CrystalSGPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JakeMT04
 * @since 06/08/2021
 */
public class AutoAddChestsCommand extends VenadeCommand {
  private final CrystalSGPlugin plugin;

  public AutoAddChestsCommand(CrystalSGPlugin plugin) {
    super("autoadd", "crystal.command.loot.chest.autoadd", null);
    this.plugin = plugin;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    if (sender.isConsole()) {
      return;
    }
    if (AddChestSubCommand.BLOCKED.get()) {
      sender.sendRawMessage(
          "§cAn auto-add operation is in progress. Please wait for it to finish.");
      return;
    }
    Player player = sender.getAsPlayer();
    BukkitPlayer bukkitPlayer = BukkitAdapter.adapt(player);
    Region region;
    try {
      region = WorldEdit.getInstance().getSessionManager().get(bukkitPlayer).getSelection();
    } catch (IncompleteRegionException e) {
      sender.sendRawMessage("§cPlease make a WorldEdit selection.");
      return;
    }
    if (region.getWorld() == null) {
      sender.sendRawMessage("§cYour selection does not have a world (somehow???).");
      return;
    }
    long vol = region.getVolume();
    sender.sendRawMessage("Processing region (scanning §b" + vol + "§f blocks)");
    AddChestSubCommand.BLOCKED.set(true);
    List<String> section =
        new ArrayList<>(
            this.plugin.getLootConfig().getStringList("chests." + region.getWorld().getName()));
    List<String> toAdd = new ArrayList<>();
    for (BlockVector3 vec : region) {
      Block block =
          BukkitAdapter.adapt(region.getWorld())
              .getBlockAt(vec.getBlockX(), vec.getBlockY(), vec.getBlockZ());
      if (block.getType() == Material.CHEST
          || block.getType() == Material.BARREL
          || block.getType() == Material.TRAPPED_CHEST) {
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        String s = x + ";" + y + ";" + z;
        if (section.contains(s)) {
          continue;
        }
        sender.sendRawMessage(
            "Found a §b"
                + block.getType().name().toLowerCase().replace("_", " ")
                + "§f at §b"
                + x
                + ", "
                + y
                + ", "
                + z
                + "§f.");
        toAdd.add(s);
      }
    }
    section.addAll(toAdd);
    this.plugin.getLootConfig().set("chests." + region.getWorld().getName(), section);
    this.plugin.saveLootConfig();
    AddChestSubCommand.BLOCKED.set(false);
    sender.sendRawMessage("Successfully registered §b" + toAdd.size() + "§f blocks.");
  }
}
