package net.venade.games.crystalsg.listener;

import lombok.RequiredArgsConstructor;
import net.venade.games.crystalsg.MiscUtils;
import net.venade.games.crystalsg.manager.GameManager;
import net.venade.games.crystalsg.player.CrystalSGPlayer;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Container;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author JakeMT04
 * @since 16/08/2021
 */
@RequiredArgsConstructor
public class PlayerInteractListener implements Listener {
  private final GameManager manager;

  @EventHandler
  public void onDamage(EntityDamageByEntityEvent event) {
    if (event.getDamager() instanceof Player) {
      if (!this.manager.getState().isStarted()
          || this.manager.getState() == GameManager.GameState.STARTING) {
        event.setCancelled(true);
        return;
      }
      CrystalSGPlayer player = this.manager.getPlayers().get(event.getDamager().getUniqueId());
      if (player == null) {
        return;
      }
      if (player.getState().isSpectating()) {
        event.setCancelled(true);
        return;
      }
      if (event.getEntity() instanceof Player) {
        if (!this.manager.isPvPEnabled()) {
          event.setCancelled(true);
          long diff = this.manager.getTimeToPvP() - System.currentTimeMillis();
          String formatted = MiscUtils.millisToTimer(diff);
          event.getDamager().sendMessage("§cPvP will enable in §f" + formatted + "§c.");
        }
      }
    }
  }

  @EventHandler
  public void onPlayerInventoryClick(InventoryClickEvent event) {
    CrystalSGPlayer player = this.manager.getPlayers().get(event.getWhoClicked().getUniqueId());
    if (player == null) {
      return;
    }
    if (event.getCurrentItem() != null) {
      if (event.getCurrentItem().getItemMeta() != null) {
        ItemMeta meta = event.getCurrentItem().getItemMeta();
        if (meta.hasDisplayName()) {
          if (meta.getDisplayName()
                  .equalsIgnoreCase(
                      player.getCorePlayer().getLanguageString("crystal.hotbar.voting"))
              || meta.getDisplayName()
                  .equalsIgnoreCase(
                      player.getCorePlayer().getLanguageString("crystal.hotbar.hub"))) {
            event.setCancelled(true);
          }
        }
      }
    }
  }

  @EventHandler
  public void waitingInteract(PlayerInteractEvent event) {
    if (this.manager.getState().isStarted()) {

      return;
    }
    if (event.getItem() == null) {
      return;
    }
    if (event.getItem().getType() == Material.PAPER) {
      this.manager.getMapVotingInventory().open(event.getPlayer());
      event.setCancelled(true);
    } else if (event.getItem().getType() == Material.DRAGON_BREATH) {
      event.setCancelled(true);
      this.manager.getPlugin().sendToHub(event.getPlayer());
    }
  }

  @EventHandler
  public void onClose(InventoryCloseEvent event) {
    if (event.getInventory().getType() == InventoryType.BARREL) {
      if (event.getInventory().getHolder() != null
          && event.getInventory().getHolder() instanceof Container) {
        if (event.getInventory().isEmpty()) {
          Location loc = ((Container) event.getInventory().getHolder()).getLocation();
          manager
              .getPlugin()
              .getServer()
              .getScheduler()
              .runTaskLater(
                  manager.getPlugin(),
                  () -> {
                    loc.getBlock().setType(Material.AIR);
                    event
                        .getPlayer()
                        .getWorld()
                        .playSound(loc, Sound.ENTITY_ZOMBIE_BREAK_WOODEN_DOOR, 1, 1);
                    event
                        .getPlayer()
                        .getWorld()
                        .playEffect(loc, Effect.STEP_SOUND, Material.BARREL);
                  },
                  2L);
        }
      }
    }
  }
}
