package net.venade.games.crystalsg.cmd;

import net.venade.games.crystalsg.manager.GameManager;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class StartGameRightNow extends VenadeCommand {
  private final GameManager manager;

  public StartGameRightNow(GameManager manager) {
    super("start", "crystal.command.start", null);
    this.manager = manager;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    if (this.manager.getState().isStarted()) {
      sender.sendRawMessage("§cThe game has already started.");
      return;
    }
    long now = System.currentTimeMillis();
    if (this.manager.getTimeToStart() != 0) {
      long diff = this.manager.getTimeToStart() - now;
      if (diff <= 15_000L) {
        sender.sendRawMessage("§cThe game will start in 5 seconds.");
        return;
      }
    }
    this.manager.setState(GameManager.GameState.WAITING_ENOUGH);
    this.manager.setTimeToStart(System.currentTimeMillis() + 16_000L);
    this.manager.getStartTask().setBroadcastNext(15);
    sender.sendRawMessage("Forced the game to start in §b00:15§f.");
  }
}
