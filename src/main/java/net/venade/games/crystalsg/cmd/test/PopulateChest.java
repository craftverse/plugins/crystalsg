package net.venade.games.crystalsg.cmd.test;

import net.venade.games.crystalsg.manager.GameManager;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.entity.Player;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class PopulateChest extends VenadeCommand {
  private final GameManager manager;

  public PopulateChest(GameManager manager) {
    super("populate", "Populates the chest you are looking at", null);
    this.manager = manager;
  }

  @CommandMethod
  public void onCommand(Sender sender) {
    if (sender.isConsole()) {
      return;
    }
    Player player = sender.getAsPlayer();
    List<Block> sightLine =
        player.getLineOfSight(EnumSet.of(Material.AIR, Material.CAVE_AIR, Material.VOID_AIR), 10);
    Optional<Block> optionalBlock =
        sightLine.stream().filter(i -> i.getType() != Material.AIR).findFirst();
    if (optionalBlock.isEmpty()) {
      sender.sendRawMessage(
          "§cYou are not looking at a block. (" + sightLine.size() + " blocks scanned)");
      return;
    }
    Block block = optionalBlock.get();
    String niceName = block.getType().name().toLowerCase().replace("_", " ");
    if (!(block.getState() instanceof Container)) {
      sender.sendRawMessage(
          "§cThe block you are looking at is not a container (" + niceName + ").");
      return;
    }
    Container chest = (Container) block.getState();
    manager.getLootGenerator().populateChest(chest, false);
    sender.sendRawMessage("Attempted to populate the §b" + niceName + "§f with the loot table.");
  }
}
